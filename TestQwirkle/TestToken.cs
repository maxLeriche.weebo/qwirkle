﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Projet_qwirkle;

namespace TestPlayer
{
    [TestClass]
    public class TestPion
    {
        //      Tests Token's Shape's Code Getter

        [TestMethod]
        public void GetShapeCodeTest()
        {
            //  Initializing a Token
            Projet_qwirkle.pion p42 = new Projet_qwirkle.pion(42);

            //  Testing Getter
            Assert.AreEqual(4, p42.get_forme());
            Assert.AreNotEqual(2, p42.get_forme());
            Assert.AreNotEqual(0, p42.get_forme());
        }

        //      Tests Token's Shape's String Getter

        [TestMethod]
        public void GetShapeStringTest()
        {
            //  Initializing a Token
            Projet_qwirkle.pion p42 = new Projet_qwirkle.pion(42);

            //  Shape String Getter
            Assert.AreEqual("♣", p42.Get_forme_string());
            Assert.AreNotEqual("♥", p42.Get_forme_string());
            Assert.AreNotEqual(" ", p42.Get_forme_string());
        }

        //      Tests Token's Color's Code Getter

        [TestMethod]
        public void GetColorCodeTest()
        {
            //  Initializing a Token
            Projet_qwirkle.pion p42 = new Projet_qwirkle.pion(42);

            //  Color Code Getter
            Assert.AreEqual(2, p42.get_couleur());
            Assert.AreNotEqual(4, p42.get_couleur());
            Assert.AreNotEqual(0, p42.get_couleur());
        }

        //      Tests Token's Shape's Code Setter

        [TestMethod]
        public void SetShapeCodeTest()
        {
            //  Initializing a Token
            Projet_qwirkle.pion p42 = new Projet_qwirkle.pion(42);

            //  Setting Value
            p42.set_forme_int(9);

            //  Testing New Value
            Assert.AreEqual(9, p42.get_forme());
            Assert.AreNotEqual(4, p42.get_forme());
            Assert.AreNotEqual(0, p42.get_forme());


            //  Try Setting an Impossible Value
            p42.set_forme_int(99);

            //  Testing New Value
            Assert.AreEqual(0, p42.get_forme());
            Assert.AreNotEqual(4, p42.get_forme());
            Assert.AreNotEqual(9, p42.get_forme());
        }

        //      Tests Token's Shape's String Setter

        [TestMethod]
        public void SetShapeStringTest()
        {
            //  Initializing a Token
            Projet_qwirkle.pion p42 = new Projet_qwirkle.pion(42);

            //  Setting Value
            p42.set_forme_string("•");

            //  Testing New Value
            Assert.AreEqual(9, p42.get_forme());
            Assert.AreNotEqual(4, p42.get_forme());
            Assert.AreNotEqual(0, p42.get_forme());


            //  Try Setting an Impossible Value
            p42.set_forme_int(99);

            //  Testing New Value
            Assert.AreEqual(0, p42.get_forme());
            Assert.AreNotEqual(4, p42.get_forme());
            Assert.AreNotEqual(9, p42.get_forme());
        }

        //      Tests Token's Color's Code Setter

        [TestMethod]
        public void SetColorCodeTest()
        {
            //  Initializing a Token
            Projet_qwirkle.pion p42 = new Projet_qwirkle.pion(42);

            //  Setting Value
            p42.set_couleur(9);

            //  Testing New Value
            Assert.AreEqual(9, p42.get_couleur());
            Assert.AreNotEqual(2, p42.get_couleur());
            Assert.AreNotEqual(0, p42.get_couleur());
            
            
            //  Try Setting an Impossible Value
            p42.set_forme_int(99);

            //  Testing New Value
            Assert.AreEqual(0, p42.get_couleur());
            Assert.AreNotEqual(2, p42.get_couleur());
            Assert.AreNotEqual(9, p42.get_couleur());
        }

        //      Tests Various Token Values for Both Constructors
        //      (Test on Shape String and Color Value)

        [TestMethod]
        public void ShapeTest()
        {
            //  Create a Token
            Projet_qwirkle.pion emptyPion_c1 = new Projet_qwirkle.pion(0);
            Assert.AreEqual(" ", emptyPion_c1.Get_forme_string());
            Assert.AreNotEqual("★", emptyPion_c1.Get_forme_string());
            Assert.AreNotEqual("♥", emptyPion_c1.Get_forme_string());

            Assert.AreEqual(0, emptyPion_c1.get_couleur());
            Assert.AreNotEqual(4, emptyPion_c1.get_couleur());
            Assert.AreNotEqual(2, emptyPion_c1.get_couleur());


            //  New Token
            Projet_qwirkle.pion emptyPion_c2 = new Projet_qwirkle.pion();
            Assert.AreEqual(" ", emptyPion_c2.Get_forme_string());
            Assert.AreNotEqual("★", emptyPion_c2.Get_forme_string());
            Assert.AreNotEqual("♥", emptyPion_c2.Get_forme_string());

            Assert.AreEqual(0, emptyPion_c2.get_couleur());
            Assert.AreNotEqual(4, emptyPion_c2.get_couleur());
            Assert.AreNotEqual(2, emptyPion_c2.get_couleur());
        

            //  New Token
            Projet_qwirkle.pion p36 = new Projet_qwirkle.pion();
            p36.set_couleur(3);
            p36.set_forme(6);
            Assert.AreEqual("♦", p36.Get_forme_string());
            Assert.AreNotEqual("★", p36.Get_forme_string());
            Assert.AreNotEqual(" ", p36.Get_forme_string());

            Assert.AreEqual(6, p36.get_couleur());
            Assert.AreNotEqual(0, p36.get_couleur());
            Assert.AreNotEqual(4, p36.get_couleur());
            Assert.AreNotEqual(1, p36.get_couleur());


            //  New Token
            Projet_qwirkle.pion p77 = new Projet_qwirkle.pion(77);
            Assert.AreEqual("▲", p77.Get_forme_string());
            Assert.AreNotEqual(" ", p77.Get_forme_string());
            Assert.AreNotEqual("♥", p77.Get_forme_string());

            Assert.AreEqual(7, p77.get_couleur());
            Assert.AreNotEqual(0, p77.get_couleur());
            Assert.AreNotEqual(4, p77.get_couleur());
            Assert.AreNotEqual(1, p77.get_couleur());


            //  New Token
            Projet_qwirkle.pion p42 = new Projet_qwirkle.pion();
            p36.set_couleur(4);
            p36.set_forme(2);
            Assert.AreEqual("♣", p42.Get_forme_string());
            Assert.AreNotEqual(" ", p42.Get_forme_string());
            Assert.AreNotEqual("∞", p42.Get_forme_string());

            Assert.AreEqual(2, p42.get_couleur());
            Assert.AreNotEqual(0, p42.get_couleur());
            Assert.AreNotEqual(4, p42.get_couleur());
            Assert.AreNotEqual(1, p42.get_couleur());


            //  New Token
            Projet_qwirkle.pion p_test = new Projet_qwirkle.pion(404);
            Assert.AreEqual(" ", p42.Get_forme_string());
            Assert.AreNotEqual("♣", p42.Get_forme_string());

            Assert.AreEqual(0, p42.get_couleur());
            Assert.AreNotEqual(4, p42.get_couleur());
        }

        //      Lock Function Test

        [TestMethod]
        public void LockTest()
        {
            // Initializing Token
            Projet_qwirkle.pion test = new Projet_qwirkle.pion();

            //  First Test
            Assert.IsFalse(test.get_lock());

            test.locking();
            
            //  Second Test, After the Function
            Assert.IsTrue(test.get_lock());
        }

        //      Empty Token Fonction Test

        [TestMethod]
        public void EmptyTest()
        {
            //  Initializing a Token that's Not Null
            Projet_qwirkle.pion p42 = new Projet_qwirkle.pion(42);

            //  First Tests
            Assert.AreEqual(4, p42.get_forme());
            Assert.AreNotEqual(0, p42.get_forme());
            Assert.AreNotEqual(" ", p42.Get_forme_string());

            Assert.AreEqual(2, p42.get_couleur());
            Assert.AreNotEqual(4, p42.get_couleur());

            //  Function
            p42.pion_vide();

            //  Last Tests
            Assert.AreEqual(0, p42.get_forme());
            Assert.AreNotEqual(4, p42.get_forme());
            Assert.AreNotEqual("♣", p42.Get_forme_string());

            Assert.AreEqual(0, p42.get_couleur());
            Assert.AreNotEqual(2, p42.get_couleur());
        }
    }
}
