﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Projet_qwirkle;

namespace QwirkleTest
{
    [TestClass]
    public class TestPJ
    {
        //      Testing Birthdate Class's Age Getter
        //      (With Month/Day)

        [TestMethod]
        public void AgeGetterTest()
        {
            //  Initializing a Date, Getting Day's Date
            Projet_qwirkle.birthdate d21_6_99 = new Projet_qwirkle.birthdate(21, 6, 1999);
            DateTime today = DateTime.Today;

            //  Testing Value
            if (today.Month > 6 || (today.Month == 6 && today.Day >= 21))
            {
                Assert.AreEqual(d21_6_99.age(), today.Year - 2000);
                Assert.AreNotEqual(d21_6_99.age(), today.Year - 2001);
            }
            else
            {
                Assert.AreEqual(d21_6_99.age(), today.Year - 2001);
                Assert.AreNotEqual(d21_6_99.age(), today.Year - 2000);
            }
        }

        //      Tests Player's Birth Getter

        [TestMethod]
        public void PlayerBirthGetterTest()
        {
            //  Initializing Player
            Projet_qwirkle.joueur JoJo = new Projet_qwirkle.joueur("JoJo", 4, 2, 1999);

            //  Testing if Value is True (and Accessible)
            Assert.AreEqual(4, JoJo.birth().Day());
        }
        
        //      Tests Birthdate Class Getters (Day, Month, Year)

        [TestMethod]
        public void BirthdateGettersTest()
        {
            Projet_qwirkle.birthdate d1_6_66 = new Projet_qwirkle.birthdate(1, 6, 1966);
            
            //  Testing Day
            Assert.AreEqual(d1_6_66.Day(), 1);
            Assert.AreNotEqual(d1_6_66.Day(), 2);
            Assert.AreNotEqual(d1_6_66.Day(), 0);

            //  Testing Month
            Assert.AreEqual(d1_6_66.Month(), 6);
            Assert.AreNotEqual(d1_6_66.Month(), 7);
            Assert.AreNotEqual(d1_6_66.Month(), 0);

            //  Testing Year
            Assert.AreEqual(d1_6_66.Year(), 1999);
            Assert.AreNotEqual(d1_6_66.Year(), 2000);
            Assert.AreNotEqual(d1_6_66.Year(), 0);
        }

        //      Tests Player's Age Setter

        [TestMethod]
        public void AgeSetterTest()
        {
            //  Initializing a Player and his Birthdate, Getting Day's Date
            Projet_qwirkle.joueur JoJo = new Projet_qwirkle.joueur("JoJo", 21, 6, 1999);
            JoJo.set_age(21, 9, 1990);
            DateTime today = DateTime.Today;

            //  Testing Values
            if (today.Month > 9 || (today.Month == 9 && today.Day >= 21))
            {
                Assert.AreEqual(JoJo.get_age(), today.Year - 1990);
                Assert.AreNotEqual(JoJo.get_age(), today.Year - 1991);
            }
            else
            {
                Assert.AreEqual(JoJo.get_age(), today.Year - 1991);
                Assert.AreNotEqual(JoJo.get_age(), today.Year - 1990);
            }
        }

        //      Tests Player Name Getter

        [TestMethod]
        public void NameGetterTest()
        {
            //  Initializing a Player
            Projet_qwirkle.joueur Bush = new Projet_qwirkle.joueur("Bush", 11, 9, 2001);
            
            //  Testing Name's Value
            Assert.AreEqual(Bush.get_pseudo(), "Bush");
            Assert.AreNotEqual(Bush.get_pseudo(), "JoJo");
        }

        //      Tests Player Name Setter

        [TestMethod]
        public void NameSetterTest()
        {
            //  Initializing a Player
            Projet_qwirkle.joueur Bush = new Projet_qwirkle.joueur("Bush", 11, 9, 2001);

            //  Changing Player's Name
            Bush.set_pseudo("JoJo");

            //  Testing Name's New Value
            Assert.AreEqual(Bush.get_pseudo(), "JoJo");
            Assert.AreNotEqual(Bush.get_pseudo(), "Bush");
        }
    }
}

