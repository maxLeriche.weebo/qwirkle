﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Projet_qwirkle;

namespace TestQwirkle
{
    [TestClass]
    public class TestGame
    {
        //      Tests Turn/Players Count Getters

        [TestMethod]
        public void TestGetter()
        {
            //  Creating Game
            int[,] set = new int[2, 10];
            joueur pj = new joueur("test", 4, 3, 2015);
            joueur[] players = new joueur[3] { pj, pj, pj };
            Partie game = new Partie(3, set, players);

            //  Getters Tests
            Assert.AreEqual(0, game.get_tour());
            Assert.AreNotEqual(1, game.get_tour());

            Assert.AreEqual(3, game.get_nbr_joueur());
            Assert.AreNotEqual(0, game.get_nbr_joueur());
            Assert.AreNotEqual(2, game.get_nbr_joueur());
        }

        //      Players Hands Test Before and Afetr Distribution

        [TestMethod]
        public void TestDistribution()
        {
            //  Creating Token, Hand Content and Game
            pion emptyPion = new pion();
            pion[] empty_hand = { emptyPion, emptyPion, emptyPion, emptyPion, emptyPion, emptyPion };
            int[,] set = new int[2, 10];
            Partie game = new Partie(2, set);

            //  Initialization Tests
            Assert.AreEqual(empty_hand, game.get_player(0).get_main());
            Assert.AreEqual(empty_hand, game.get_player(1).get_main());

            //  Function
            game.distribution();

            //  Second Tests
            Assert.AreNotEqual(empty_hand, game.get_player(0).get_main());
            Assert.AreNotEqual(empty_hand, game.get_player(1).get_main());
        }
    }
}
