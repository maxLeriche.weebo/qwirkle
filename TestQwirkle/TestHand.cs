﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Projet_qwirkle;

namespace TestPlayer
{
    [TestClass]
    public class TestMain
    {
        //      Initial Score Value Test

        [TestMethod]
        public void ScoreInitTest()
        {
            //  Initializing Hand
            Projet_qwirkle.main mainJoJo = new Projet_qwirkle.main();

            //  Testing Score's Value at Initialization
            Assert.AreEqual(0, mainJoJo.get_score());
            Assert.AreNotEqual(1, mainJoJo.get_score());
            Assert.AreNotEqual(-1, mainJoJo.get_score());
        }

        //      Tests Score Value After Modification

        [TestMethod]
        public void ScoreUpdateTest()
        {
            //  Initializing Hand
            Projet_qwirkle.main mainJoJo = new Projet_qwirkle.main();

            //  Testing if the Function Adds the Score
            mainJoJo.ajout_score(42);
            Assert.AreEqual(42, mainJoJo.get_score());
            Assert.AreNotEqual(0, mainJoJo.get_score());
            Assert.AreNotEqual(1, mainJoJo.get_score());
            Assert.AreNotEqual(43, mainJoJo.get_score());

            //  Testing if the Function Resets teh Score
            mainJoJo.ajout_score(3);
            Assert.AreEqual(45, mainJoJo.get_score());
            Assert.AreNotEqual(0, mainJoJo.get_score());
            Assert.AreNotEqual(3, mainJoJo.get_score());
            Assert.AreNotEqual(42, mainJoJo.get_score());
        }

        //      Hand Initialisation Test,
        //      Hand Value Test After Modification

        [TestMethod]
        public void HandTest()
        {
            //  Initializing Hand
            Projet_qwirkle.main mainJoJo = new Projet_qwirkle.main();
            mainJoJo.init("JoJo", 1, 2, 2000);

            // Creating Tokens
            Projet_qwirkle.pion emptyPion = new Projet_qwirkle.pion(0); //  Empty
            Projet_qwirkle.pion p32 = new Projet_qwirkle.pion(32);      //  Blue ♦
            Projet_qwirkle.pion p77 = new Projet_qwirkle.pion(77);      //  Pink ▲

            //  Creating 3 Hands, the First Being Empty
            pion[] empty_hand = { emptyPion, emptyPion, emptyPion, emptyPion, emptyPion, emptyPion };
            pion[] expected = { p32, p32, p32, p32, p77, p32 };
            pion[] not_expected = { p77, p32, p77, p77, p32, emptyPion };

            //  Testing if Hand's Initialization is Correct
            Assert.AreEqual(empty_hand, mainJoJo.get_main());
            Assert.AreNotEqual(not_expected, mainJoJo.get_main());


            mainJoJo.set_main(expected);

            //  Testing if the Hand's Value Has Changed
            Assert.AreEqual(expected, mainJoJo.get_main());
            Assert.AreNotEqual(not_expected, mainJoJo.get_main());
            Assert.AreNotEqual(empty_hand, mainJoJo.get_main());
        }

        //      Testing Hand's Player Functions Accessibility

        [TestMethod]
        public void HandPlayerAccessTest()
        {
            //  Initializing Hand
            Projet_qwirkle.main mainJoJo = new Projet_qwirkle.main();
            //  Initialising Player
            mainJoJo.init("JoJo", 4, 2, 1999);

            //  Testing if Hand's Player's Values Are True (and Accessible)
            Assert.AreEqual("JoJo", mainJoJo.get_player().get_pseudo());
            Assert.AreNotEqual("Bush", mainJoJo.get_player().get_pseudo());

            Assert.AreEqual(4, mainJoJo.get_player().birth().Day());
            Assert.AreNotEqual(2, mainJoJo.get_player().birth().Day());
        }
    }
}
