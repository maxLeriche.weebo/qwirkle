﻿using System;

namespace Projet_qwirkle
{
    public class Pioche
    {
        private int nbr_pions=0;
        private int[,] pions_dispo = new int[10,10];    //Deck's Content


        //  Constructor
        public Pioche(int[,] difficulté)
        {
            for (int iteration = 0; iteration < 9; iteration++)
            {
                pions_dispo[iteration+1, 0] = difficulté[iteration, 0];
                pions_dispo[0, iteration+1] = difficulté[iteration, 1];
            }

            for (int TabX = 1; TabX < 10; TabX++)
            {
                if (pions_dispo[TabX, 0] == 1)
                {
                    for (int TabY = 1; TabY <= 9; TabY++)
                    {// X/Y are Color and Shape.
                        if (pions_dispo[0, TabY] == 1)
                        {//  If They're Both Activated (difficulté var), the Matching Cell Will be Filled by 3, the Number in the Deck
                            pions_dispo[TabX, TabY] = 3;
                        }
                    }   
                }   
            }
        }

        //  Getters
        public int get_nbr_pions()      { return this.nbr_pions; }
        public int[,] get_pions_dispo() { return this.pions_dispo; }

        public void retour_pions(pion objet)
        {
            pions_dispo[objet.get_couleur(), objet.get_forme()] += 1;
        }

        public pion tirage_pions()
        {
            pion retour = new pion();
            //pion test;
            Random RandGenerator = new Random();
            int axecouleur = 0, axeforme = 0;

            do
            {
                axecouleur = RandGenerator.Next() % 9 + 1;
                axeforme = RandGenerator.Next() % 9 + 1;

                axecouleur = tirage_pions_couleur(axecouleur);
                axeforme = tirage_pions_forme(axeforme);
            } while (this.pions_dispo[axecouleur, axeforme] == 0);
            
            

            //if (this.pions_dispo[axecouleur, axeforme] == 0)
            //{
            //    test = tirage_pions();
            //    axecouleur = test.get_couleur();
            //    axeforme = test.get_forme();
            //}

            this.pions_dispo[axecouleur, axeforme] -= 1;
            retour.set_forme(axeforme);
            retour.set_couleur(axecouleur);

            return retour;
        }

        private int tirage_pions_couleur(int axecouleur)
        {
            int swap =0;
            for (; pions_dispo[axecouleur, 0] != 1&&axecouleur!=swap; axecouleur++)
            {
                if (axecouleur == 9)
                {
                    swap = axecouleur;
                    axecouleur = 1;
                }
            }

            return axecouleur;
        }

        private int tirage_pions_forme(int axeforme)
        {
            int swap = 0;
            for ( ; pions_dispo[0, axeforme] != 1&&axeforme!=swap; axeforme++)
            {
                if (axeforme == 9)
                {
                    swap = axeforme;
                    axeforme = 1;
                }
                
            }

            return axeforme;
        }

        public bool intégrité_pioche()
        {
            int swap = 0;
            for (int iterationx = 1;iterationx<10;iterationx++)
            {
                for (int iterationy = 1; iterationy<10;iterationy++)
                {
                    swap += pions_dispo[iterationx, iterationy];
                }
            }
            this.nbr_pions = swap;

            if (this.nbr_pions>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
