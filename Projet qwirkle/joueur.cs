﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Projet_qwirkle
{
    public class birthdate
    {
        int day;
        int month;
        int year;

        DateTime today = DateTime.Today;

        //  Constructor
        public birthdate(int day, int month, int year)
        {
            this.day = day; this.month = month; this.year = year;
        }

        //  Getters
        public int Day()        { return day; }
        public int Month()      { return month; }
        public int Year()       { return year; }

        public int age()
        {
            int age = today.Year-year;

            if (today.Month > this.month || (today.Month == this.month && today.Day >= this.day))
            {
                age += 1;
            }
            return age;
        }
    }

    public class joueur
    {
        private string pseudo;
        private birthdate age;
        //public img photo ;


        // Constructor
        public joueur(string pseudo, int day,int month,int year)
        {
            this.pseudo = pseudo;
            age = new birthdate(day, month, year);
        }
        public joueur(string pseudo)
        {
            this.pseudo = pseudo;
        }

        //  Getters
        public string get_pseudo()                          { return this.pseudo; }
        public birthdate birth()                            { return this.age; }
        public int get_age()                                { return age.age(); }

        //  Setters
        public void set_pseudo(string pseudo)               { this.pseudo = pseudo; }
        public void set_age(int day,int month,int year )    { this.age = new birthdate(day, month, year); }
        
        /*
        public void set_photo(img photo)    { this.photo = photo; }
        public img get_photo()              { return this.photo; }
         */
    }
}
