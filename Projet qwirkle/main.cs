﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projet_qwirkle
{
    public class main
    {
        private pion[] Main = new pion[6];  //  The Hand, 6 Slots
        private int score = 0;              //  Player's Score
        private int score_dernier_tour = 0;
        public joueur player;              //  Hand's Owner
        
        //  (We Manipulate the Hand, Not the Player)
        public  main()
        {
            remplissage();
        }
        private void remplissage()
        {
            for (int iteration = 0; iteration < 6; iteration++)
            {
                this.Main[iteration] = new pion();
            }
        }

        public void init(joueur joueur_swap)
        {
            player = joueur_swap;
        }
        
        //  Getters
        public pion[] get_main()                { return this.Main; }
        public int get_score()                  { return this.score; }
        public joueur get_player()              { return this.player; }
        public int get_score_dernier_tour() { return score_dernier_tour; }

        //  Setters
        public void ajout_score(int points)     { this.score += points; }
        public void set_main(pion[] Main)       { this.Main = Main; }
        public void set_score_dernier_tour(int score) { this.score_dernier_tour = score; }  
        public int poids_debut_partie_main() {
            int weight = 0;
            List<pion>[] couleur = poids_debut_partie_main_remplissage_list_couleur();
            List<pion>[] forme = poids_debut_partie_main_remplissage_list_forme();
            int[] poidscouleur =traitement_couleur(couleur);
            int[] poidsforme =traitement_forme(forme);
            foreach(int poids in poidscouleur)
            {
                if (poids>weight) { weight = poids; }
            }
            foreach (int poids in poidsforme)
            {
                if (poids > weight) { weight = poids; }
            }
            return weight;
        }

        private List<pion>[] poids_debut_partie_main_remplissage_list_couleur()
        {
            List<pion>[] couleur = new List<pion>[9];
            for (int remplissage =0; remplissage<9;remplissage++)
            {
                couleur[remplissage] = new List<pion>();
            }
            for (int iteration = 0; iteration < 6; iteration++)
            {
                switch (Main[iteration].get_couleur())
                {
                    case 1:
                        couleur[0].Add(Main[iteration]);
                        break;
                    case 2:
                        couleur[1].Add(Main[iteration]);
                        break;
                    case 3:
                        couleur[2].Add(Main[iteration]);
                        break;
                    case 4:
                        couleur[3].Add(Main[iteration]);
                        break;
                    case 5:
                        couleur[4].Add(Main[iteration]);
                        break;
                    case 6:
                        couleur[5].Add(Main[iteration]);
                        break;
                    case 7:
                        couleur[6].Add(Main[iteration]);
                        break;
                    case 8:
                        couleur[7].Add(Main[iteration]);
                        break;
                    case 9:
                        couleur[8].Add(Main[iteration]);
                        break;
                }
            }
            return couleur;
        }
        private List<pion>[] poids_debut_partie_main_remplissage_list_forme()
        {
            List<pion>[] forme = new List<pion>[9];
            for (int remplissage = 0; remplissage < 9; remplissage++)
            {
                forme[remplissage] = new List<pion>();
            }
            for (int iteration = 0; iteration < 6; iteration++)
            {
                switch (Main[iteration].get_couleur())
                {
                    case 1:
                        forme[0].Add(Main[iteration]);
                        break;
                    case 2:
                        forme[1].Add(Main[iteration]);
                        break;
                    case 3:
                        forme[2].Add(Main[iteration]);
                        break;
                    case 4:
                        forme[3].Add(Main[iteration]);
                        break;
                    case 5:
                        forme[4].Add(Main[iteration]);
                        break;
                    case 6:
                        forme[5].Add(Main[iteration]);
                        break;
                    case 7:
                        forme[6].Add(Main[iteration]);
                        break;
                    case 8:
                        forme[7].Add(Main[iteration]);
                        break;
                    case 9:
                        forme[8].Add(Main[iteration]);
                        break;
                }
            }
            return forme;
        }

        private int[] traitement_couleur(List<pion>[] couleur)
        {
            int[] poids = new int[9] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            for(int iteration=0;iteration<9;iteration++)
            {
                if (couleur[iteration].Count > 1)
                {
                    List<pion> poidscouleur = new List<pion>();
                    for (int remplissage = 0; remplissage < 9; remplissage++)
                    {
                        couleur[remplissage] = new List<pion>();
                    }
                    int index=0;
                    foreach(pion temporaire in couleur[iteration])
                    {
                        bool test = true;
                        int iteration2 = 0;
                        do
                        { 
                            if (temporaire.get_forme()==poidscouleur[iteration2].get_forme()){ test = false;}
                            iteration2++;
                        } while (iteration2 < index&&test==true);
                        if (test)
                        {
                            poidscouleur.Add(temporaire);
                            index ++;
                        }
                    }
                    poids[iteration] = poidscouleur.Count;
                }
            }
            return poids;
        }
        private int[] traitement_forme(List<pion>[] forme)
        {
            int[] poids = new int[9] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            for (int iteration = 0; iteration < 9; iteration++)
            {
                if (forme[iteration].Count>1)
                {
                    List<pion> poidsforme = new List<pion>();
                    for (int remplissage = 0; remplissage < 9; remplissage++)
                    {
                        forme[remplissage] = new List<pion>();
                    }
                    int index = 0;
                    foreach (pion temporaire in forme[iteration])
                    {
                        bool test = true;
                        int iteration2 = 0;
                        do
                        {
                            if (temporaire.get_couleur() == poidsforme[iteration2].get_couleur()) { test = false; }
                            iteration2++;
                        } while (iteration2 < index && test == true);
                        if (test)
                        {
                            poidsforme.Add(temporaire);
                            index++;
                        }
                    }
                    poids[iteration] = poidsforme.Count;
                }
            }
            return poids;
        }
    }
}
