﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projet_qwirkle
{
    public class Partie
    {
        private int nbr_joueur;
        private int nbr_tour=0;
        //private int id_partie;
        private int tour_joueur=0;
        private int[,] dificulté = new int[9,2];
        private int[] nbrcouleur_forme = new int[2];
        private List<main> player=new List<main>();
        private List<main> Joueur_dans_lordre;
        private Pioche pioche;
        private Plateau plateau;

        public Partie(int nbr_joueur, int[,]dificulte,joueur[] tab_de_joueur)
        {
            //Initializing Class and Instances
            this.nbr_joueur = nbr_joueur;
            this.dificulté = dificulte;
            this.pioche = new Pioche(dificulte);
            this.plateau = new Plateau(nbrcouleur_forme);

            for (int joueur =0;joueur<nbr_joueur;joueur++)
            {
                main swapjoueur = new main();
                swapjoueur.init(tab_de_joueur[joueur]);
                player.Add(swapjoueur);
                this.player[joueur].set_main(distribution(this.player[joueur]));
            }
            mise_en_ordre_joueur();
            for(int iteration = 0; iteration < 9; iteration++)
            {
                nbrcouleur_forme[0]+= dificulté[iteration, 0];
                nbrcouleur_forme[1] += dificulté[iteration, 1];
            }
            
        }

        public string get_pseudo(int nbrjoueur)
        {
            return Joueur_dans_lordre[nbrjoueur].player.get_pseudo();
        }

        public pion[] distribution(main play)
        {
            pion[] swap = new pion[6];
            for (int iteration = 0; iteration < 6; iteration++)
            {
                swap[iteration] = new pion();
            }
            //Players Hands Fill
            for (int iteration =0;iteration<6;iteration++)
            {
                
                if (swap[iteration].get_couleur()==0)
                {
                    swap[iteration] = play.get_main()[iteration];
                    swap[iteration] = pioche.tirage_pions();
                }
            }
            return swap;
        }

        
        public Plateau get_class_plateau() { return this.plateau; }
        public Pioche get_class_pioche() { return this.pioche; }
        public main get_player_pas_dans_lordre(int slot)    { return this.player[slot-1]; }
        public main get_player(int slot) { return this.Joueur_dans_lordre[slot - 1]; }
        public main get_qui_joue_main() { return Joueur_dans_lordre[tour_joueur]; }
        public int get_qui_joue_int()           { return this.tour_joueur; }
        public int get_nbr_joueur()         { return this.nbr_joueur; }
        public int get_tour() { return this.nbr_tour; }

        //public int get_id()                 { return this.id_partie; }

        public void mise_en_ordre_joueur()
        {
            int[] poids = new int[nbr_joueur];
            for (int iteration = 0; iteration < nbr_joueur; iteration++)
            {
               poids[iteration]= player[iteration].poids_debut_partie_main();
            }
            Joueur_dans_lordre = new List<main>();
            int temporaire_max=0;
            for(int mise_en_ordre_1 =0;mise_en_ordre_1<nbr_joueur;mise_en_ordre_1++)
            {
                for (int mise_en_ordre_2=0;mise_en_ordre_2<nbr_joueur;mise_en_ordre_2++)
                {
                    if (poids[mise_en_ordre_2]>poids[temporaire_max])
                    {
                        temporaire_max = mise_en_ordre_2;
                    }
                }
                Joueur_dans_lordre.Add(player[temporaire_max]);
                poids[temporaire_max] = -1;
            }
        }

        public void fin_tour() 
        {
            int score;
            this.nbr_tour = (nbr_tour + 1) ;
            this.tour_joueur = (nbr_tour) % nbr_joueur;
            score =plateau.calcule_point(nbr_tour);
            if (score == -1)
            {

            }
            plateau.fin_tour_plateau();
            Joueur_dans_lordre[tour_joueur].set_score_dernier_tour(score);
            Joueur_dans_lordre[tour_joueur].ajout_score(score);

        }

        public main[] fin_partie()
        {
            main[] retour = new main[nbr_joueur];
            main swap1 = player[0];
            List<main> swap2 = this.player;
            int position = 0;

            //Points Order Creation 
            for (int iteration2=0;iteration2<nbr_joueur-1;iteration2++)
            {
                for (int iteration = iteration2; iteration <= nbr_joueur - 1; iteration++)
                {
                    if (swap1.get_score() < swap2[iteration].get_score())
                    {
                        swap1 = swap2[iteration];
                        position = iteration;
                    }
                }
                retour[iteration2] = swap2[position];
            }
            return retour;
        }

        
    }
}
