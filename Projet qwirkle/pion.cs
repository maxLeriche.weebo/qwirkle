﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Projet_qwirkle
{
    public class pion
    {
        private int forme = 0, couleur = 0; //  Color and Shape's Codes
        private bool verouille = false;

        //  Getters
        public int get_forme()                  { return this.forme; }
        public int get_couleur()                { return this.couleur; }
        public bool get_lock()                  { return this.verouille; }

        //  Setters
        public void set_forme(int forme)        { this.forme = forme; }
        public void set_couleur(int couleur)    { this.couleur = couleur; }

        //  Functions
        public void locking()                   { this.verouille = true; }
        public void pion_vide()                 { this.forme = 0; this.couleur = 0; }

        //  Constructors
        public pion()
        {

        }

        public pion(int value)
        {//  Testing if Argument is Correct
            if (value < 100 && value > 10 && value%10 != 0)
            {
                couleur = value % 10;
                forme = (value - couleur) % 10;
            }

        }
        

        //  Other Getters
        public string Get_forme_string()
        {
            switch (this.forme)
            {
                case 1:
                    return "■";
                case 2:
                    return "♥";
                case 3:
                    return "♦";
                case 4:
                    return "♣";
                case 5:
                    return "♠";
                case 6:
                    return "★";
                case 7:
                    return "▲";
                case 8:
                    return "∞";
                case 9:
                    return "•";
            }
            return " ";
        }

        public Color Get_couleur_colo()
        {
            switch(this.couleur)
            {
                case 1:
                    return Color.Red;
                case 2:
                    return Color.Blue;
                case 3:
                    return Color.Green;
                case 4:
                    return Color.Brown;
                case 5:
                    return Color.Yellow;
                case 6:
                    return Color.Orange;
                case 7:
                    return Color.Pink;
                case 8:
                    return Color.Purple;
                case 9:
                    return Color.Indigo;
            }
            return Color.White;
        }

        //  Other Setters
        public void set_forme_string(string forme)
        {
            switch (forme)
            {
                case "■":
                    this.forme = 1;
                    break;
                case "♥":
                    this.forme = 2;
                    break;
                case "♦":
                    this.forme = 3;
                    break;
                case "♣":
                    this.forme = 4;
                    break ;
                case "♠":
                    this.forme = 5;
                    break;
                case "★":
                    this.forme = 6;
                    break;
                case "▲":
                    this.forme = 7;
                    break;
                case "∞":
                    this.forme = 8;
                    break;
                case "•":
                    this.forme = 9;
                    break ;
                case " ":
                    this.forme = 0;
                    break;
                default:
                    break;
            }
        }

        public void set_couleur_colo(Color couleur)
        {
            if (couleur == Color.Red)
            {
                this.couleur = 1;
            }
            else if (couleur ==Color.Blue)
            {
                this.couleur = 2;
            }
            else if (couleur==Color.Green)
            {
                this.couleur = 3;
            }
            else if (couleur==Color.Brown)
            {
                this.couleur = 4;
            }
            else if (couleur==Color.Yellow)
            {
                this.couleur = 5;
            }
            else if (couleur==Color.Orange)
            {
                this.couleur = 6;
            }
            else if (couleur ==Color.Pink)
            {
                this.couleur = 7;
            }
            else if (couleur==Color.Purple)
            {
                this.couleur = 8;
            }
            else if (couleur==Color.Indigo)
            {
                this.couleur = 9;
            }
            else
            {
                this.couleur = 0;
            }
        }

        public static bool operator ==(pion pion1,pion pion2)
        {
            if (pion1.couleur==pion2.couleur&&pion1.forme==pion2.forme)
            {
                return true;
            }
            return false;
        }
        public static bool operator !=(pion pion1, pion pion2)
        {
            if (pion1.couleur != pion2.couleur && pion1.forme != pion2.forme)
            {
                return true;
            }
            return false;
        }

    }
}
