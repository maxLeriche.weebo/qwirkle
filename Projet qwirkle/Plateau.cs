﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Projet_qwirkle
{
    public class Plateau
    {
        private pion[,] plateau = new pion[30, 30];
        private pion[,] plateau_swap = new pion[30, 30];
        private int[] dimension = new int[2] { 11, 11 };
        private int[,] pion_poser = new int[2, 6];
        private int nbr_pion_poser = -1;
        private int axe = -1; //x = 0 y =1
        private int[] dificulte;

        public Plateau(int[] dificulté)
        {
            for (int iterationx=0;iterationx<30;iterationx++)
            {
                for (int iterationy = 0; iterationy < 30; iterationy++)
                {
                    pion temporaire = new pion();
                    pion temporaire2 = new pion();
                    plateau[iterationx, iterationy] = temporaire;
                    plateau_swap[iterationx, iterationy] = temporaire2;
                }
            }
            this.dificulte = dificulté;
        }

        //  Getters
        public pion[,] get_plateau() { return this.plateau; }
        public pion[,] get_plateau_swap() { return this.plateau_swap; }
        public int[] get_dimension() { return this.dimension; }
        public int[,] get_pion_poser() { return this.pion_poser; }

        public void set_pion_poser_0()
        {
            for (int iterationx = 0; iterationx < 2; iterationx++)
            {
                for (int iterationy = 0; iterationy < 6; iterationy++)
                {
                    this.pion_poser[iterationx, iterationy] = 0;
                }
            }
            this.nbr_pion_poser = 0;
            axe = -1;
        }

        #region calcul score
        public int calcule_point(int nbrtour)
        {
            int score = 0;
            if (this.nbr_pion_poser==0) {  return -1; }
            if (nbrtour ==0)
            {
                if (this.nbr_pion_poser==dificulte[0]) { return nbr_pion_poser + nbr_pion_poser; }
                else { return nbr_pion_poser; }
            }
            
            return quelle_pion();
        }

        private int quelle_pion()
        {
            int score = 0, qwikle = 0;
            for (int iterationx = 0; iterationx < 30; iterationx++)
            {
                for (int iterationy = 0; iterationy < 30; iterationy++)
                {
                    if (plateau_swap[iterationx, iterationy].get_lock() == false)
                    {
                        qwikle++;
                        test_pion(iterationx, iterationy);

                    }
                }
            }

            return score;
        }
        

        private int test_pion(int coox,int cooy)
        {
            int axex = 0, axey = 0, retour = 0;
            for(int iteration =0; plateau_swap[coox + iteration, cooy].get_lock() && (coox + iteration) <=29; iteration++) { axex++; }
            for (int iteration = 0; plateau_swap[coox - iteration, cooy].get_lock()&&(coox-iteration)>=0; iteration++) { axex++; }
            for (int iteration = 0; plateau_swap[coox , cooy + iteration].get_lock() && (cooy + iteration) <= 29; iteration++) { axey++; }
            for (int iteration = 0; plateau_swap[coox , cooy - iteration].get_lock() && (cooy - iteration) >= 0; iteration++) { axey++; }

            if (axex == dificulte[1]) { retour += dificulte[1]; }
            if (axey == dificulte[1]) { retour += dificulte[1]; }
            if (axex > dificulte[1]) { throw new NotImplementedException(); }
            return retour + axex + axey;
        }
        

        #endregion


        #region redimensionnement
        public pion[,] get_plateau_redimensionne()
        {
            redimensionnement();
            pion[,] plateau_redimensionne = new pion[dimension[0], dimension[1]];
            for (int iterationx = 0; iterationx < dimension[0]; iterationx++)
            {
                for (int iterationy = 0; iterationy < dimension[1]; iterationy++)
                {
                    plateau_redimensionne[iterationx, iterationy] = plateau[iterationx, iterationy];
                }
            }
            return plateau_redimensionne;
        }
        public void redimensionnement()
        {
            if (dimension[0] < 30)
            {
                somme_axeX();
                somme_axeX1();
            }
            if (dimension[1] < 30)
            {
                somme_axeY();
                somme_axeY1();
            }
        }

        private int somme_axeX()
        {
            int retour = 0;

            for (int iteration = 0; iteration < dimension[1]; iteration++)
            {
                if (plateau_swap[0, iteration].get_forme() != 0)
                {
                    retour += 1;
                }
            }
            if (retour > 0)
            {
                redimensionnement_axeX();
            }

            return retour;
        }

        private int somme_axeY()
        {
            int retour = 0;

            for (int iteration = 0; iteration < dimension[1]; iteration++)
            {
                if (plateau_swap[iteration, 0].get_forme() != 0)
                {
                    retour += 1;
                }
            }
            if (retour > 0)
            {
                redimensionnement_axeY();
            }

            return retour;
        }

        private int somme_axeX1()
        {
            int retour = 0;

            for (int iteration = 0; iteration < dimension[1]; iteration++)
            {
                if (plateau_swap[dimension[0] - 1, iteration].get_forme() != 0)
                {
                    retour += 1;
                }
            }
            if (retour > 0)
            {
                dimension[0] += 1;
            }
            return retour;
        }
        private int somme_axeY1()
        {
            int retour = 0;

            for (int iteration = 0; iteration < dimension[1]; iteration++)
            {
                if (plateau_swap[iteration, dimension[1] - 1].get_forme() != 0)
                {
                    retour += 1;
                }
            }
            if (retour > 0)
            {
                dimension[1] += 1;
            }
            return retour;
        }

        private void redimensionnement_axeX()
        {
            this.dimension[0] += 1;
            for (int iterationx = this.dimension[0] - 2; iterationx >= 0; iterationx--)
            {
                for (int iterationy = this.dimension[1] - 1; iterationy >= 0; iterationy--)
                {
                    this.plateau[iterationx + 1, iterationy] = this.plateau[iterationx, iterationy];
                    this.plateau[iterationx, iterationy] = new pion();
                }
            }
        }
        private void redimensionnement_axeY()
        {
            this.dimension[1] += 1;
            for (int iterationX = this.dimension[1] - 2; iterationX >= 0; iterationX--)
            {
                for (int iterationY = this.dimension[0] - 1; iterationY >= 0; iterationY--)
                {
                    this.plateau[iterationY, iterationX + 1] = this.plateau[iterationY, iterationX];
                    this.plateau[iterationY, iterationX] = new pion();
                }
            }
        }
        #endregion

        #region test_placement
        public bool test_placement(int coox, int cooy, pion objet)
        {
            bool retour = false;
            if (nbr_pion_poser == 0) { retour = test_placement_main(coox, cooy, objet); }
            else if (nbr_pion_poser == 1)
            {
                redimensionnement();
                retour = test_pion_poser1(coox, cooy, objet);
            }
            else if (nbr_pion_poser == -1)
            {
                redimensionnement();
                return test_pion_poser_minus1(coox, cooy, objet);
            }
            else
            {
                retour = test_pion_poserelse(coox, cooy, objet);

                retour = false;
            }
            redimensionnement();
            return retour;
        }
        
        private bool test_pion_poser1(int coox, int cooy, pion objet)
        {
            bool retour;
            if (plateau[coox, cooy].get_lock() == true) { return false; }
            retour = test_placement_main(coox, cooy, objet);
            if (retour == true)
            {
                if (coox == pion_poser[0, 0])
                {
                    axe = 0;
                }
                else if (cooy == pion_poser[1, 0])
                {
                    axe = 1;
                }
            }
            return retour;
        }
        private bool test_pion_poser_minus1(int coox, int cooy, pion objet)
        {
            this.plateau_swap[5, 5] = objet;
            nbr_pion_poser = 1;
            pion_poser[0, 0] = 5;
            pion_poser[0, 1] = 5;
            return true;
        }
        private bool test_pion_poserelse(int coox, int cooy, pion objet)
        {
            bool retour = false;
            if (axe == 0)
            {
                if (coox == pion_poser[0, 0]) { retour = test_placement_main(coox, cooy, objet); }
                else { retour = false; }
            }
            else if (axe == 1)
            {
                if (cooy == pion_poser[1, 0]) { retour = test_placement_main(coox, cooy, objet); }
                else { retour = false; }
            }
            return retour;
        }

        private bool test_placement_main(int coox, int cooy, pion objet)
        {
            int x1 = 0, x2 = 0, y1 = 0, y2 = 0;

            x1 = test_haut(coox, cooy, objet);
            x2 = test_bas(coox, cooy, objet);
            y1 = test_gauche(coox, cooy, objet);
            y2 = test_droite(coox, cooy, objet);

            if (x1 == 111 || x2 == 111 || y1 == 111 || y2 == 111) { return false; }

            else if (x1 == -1 || x2 == -1 || y1 == -1 || y2 == -1) { return false; }

            if (y1 >= 1 && y2 >= 1) { test_horizontal(coox, cooy, objet); }
            if (x1 >= 1 && x2 >= 1) { test_vertical(coox, cooy, objet); }

            if ((x2 == 0 && x1 == 0 && y2 == 0 && y1 == 0)) { return false; }
            plateau_swap[coox, cooy] = objet;
            nbr_pion_poser += 1;
            pion_poser[0, 0] = coox;
            pion_poser[0, 1] = cooy;
            return true;
        }

        private int test_haut(int coox, int cooy, pion objet)
        {
            int x = 0;
            if (plateau_swap[coox + 1, cooy].get_couleur() != 0)
            {
                x = 1;
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox - 1, cooy].get_couleur())
                {
                    x += 10;
                    for (int iteration = 2; ((plateau_swap[coox + iteration, cooy].get_couleur() != 0 && plateau_swap[coox + iteration, cooy].get_forme() != 0)); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox + iteration, cooy].get_couleur())
                        {
                            return -1;
                        }
                    }
                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox + 1, cooy].get_forme())
                {
                    x += 100;
                    for (int iteration = 2; ((plateau_swap[coox + iteration, cooy].get_couleur() != 0 && plateau_swap[coox + iteration, cooy].get_forme() != 0)); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox + iteration, cooy].get_forme())
                        {
                            return -1;
                        }
                    }
                }
            }
            return x;
        }

        private int test_bas(int coox, int cooy, pion objet)
        {
            int x;
            {
                x = 1;
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox - 1, cooy].get_couleur())
                {
                    x += 10;
                    for (int iteration = 2; ((plateau_swap[coox - iteration, cooy].get_couleur() != 0 && plateau_swap[coox - iteration, cooy].get_forme() != 0)); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox - iteration, cooy].get_couleur())
                        {
                            return -1;
                        }
                    }
                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox - 1, cooy].get_forme())
                {
                    x += 100;
                    for (int iteration = 2; ((plateau_swap[coox - iteration, cooy].get_couleur() != 0 && plateau_swap[coox - iteration, cooy].get_forme() != 0)); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox - iteration, cooy].get_forme())
                        {
                            return -1;
                        }
                    }
                }
            }
            return x;
        }

        private int test_gauche(int coox, int cooy, pion objet)
        {
            int y;
            y = 1;
            if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox, cooy - 1].get_couleur())
            {
                y += 10;
                for (int iteration = 2; ((plateau_swap[coox, cooy - iteration].get_couleur() != 0 && plateau_swap[coox, cooy - iteration].get_forme() != 0)); iteration++)
                {
                    if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox, cooy - iteration].get_couleur())
                    {
                        return -1;
                    }
                }
            }
            if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox, cooy - 1].get_forme())
            {
                y += 100;
                for (int iteration = 2; ((plateau_swap[coox, cooy - iteration].get_forme() != 0 && plateau_swap[coox, cooy - iteration].get_forme() != 0)); iteration++)
                {
                    if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox, cooy - iteration].get_forme())
                    {
                        return +1;
                    }
                }
            }
            return y;
        }

        private int test_droite(int coox, int cooy, pion objet)
        {
            int y = 0;

            {
                y = 1;

                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox, cooy - 1].get_couleur())
                {
                    y += 10;
                    for (int iteration = 2; ((plateau_swap[coox, cooy + iteration].get_couleur() != 0 && plateau_swap[coox, cooy + iteration].get_forme() != 0)); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox, cooy + iteration].get_couleur())
                        {
                            return -1;
                        }
                    }

                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox, cooy + 1].get_forme())
                {
                    y += 100;
                    for (int iteration = 2; ((plateau_swap[coox, cooy + iteration].get_couleur() != 0 && plateau_swap[coox, cooy + iteration].get_forme() != 0)); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox, cooy + iteration].get_forme())
                        {
                            return -1;
                        }
                    }
                }
                return y;
            }
        }

        private bool test_vertical(int coox, int cooy, pion objet)
        {

            for (int iteration = 1; plateau_swap[coox - iteration, cooy].get_couleur() == 0; iteration++)
            {
                for (int iteration2 = 1; plateau_swap[coox + iteration2, cooy].get_couleur() == 0; iteration2++)
                {
                    if ((plateau_swap[coox + iteration2, cooy].get_couleur() == plateau_swap[coox - iteration, cooy].get_couleur()) && (plateau_swap[coox + iteration2, cooy].get_forme() == plateau_swap[coox - iteration, cooy].get_forme()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool test_horizontal(int coox, int cooy, pion objet)
        {
            for (int iteration = 1; plateau_swap[coox, cooy - iteration].get_couleur() == 0; iteration++)
            {
                for (int iteration2 = 1; plateau_swap[coox, cooy + iteration2].get_couleur() == 0; iteration2++)
                {
                    if ((plateau_swap[coox, cooy + iteration2].get_couleur() == plateau_swap[coox, cooy - iteration].get_couleur()) && (plateau_swap[coox, cooy + iteration2].get_forme() == plateau_swap[coox, cooy - iteration].get_forme()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion

        #region action tour
        public void retirer_pion(int coox,int cooy)
        {
            plateau_swap[coox, cooy].pion_vide();
        }
        public void annule_tour()
        {
            set_pion_poser_0();
            for (int iteration = 0; iteration < 30; iteration++)
            {
                for (int iteration2 = 0; iteration2 < 30; iteration2++)
                {
                    this.plateau_swap[iteration, iteration2] = this.plateau[iteration, iteration2];

                }
               
            }
        }

        public void fin_tour_plateau()
        {
            this.plateau_swap = this.plateau;
            set_pion_poser_0();
            for (int iteration = 0; iteration < 30; iteration++)
            {
                for (int iteration2 = 0; iteration2 < 30; iteration2++)
                {
                    this.plateau[iteration, iteration2] = this.plateau_swap[iteration, iteration2];
                    this.plateau[iteration, iteration2].locking();
                }

            }
        }
        #endregion
    }
}
