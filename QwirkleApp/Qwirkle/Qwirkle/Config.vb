﻿Imports Projet_qwirkle

Public Class Config
    Private rules As New Options
    Public Token(,) As Integer = New Integer(8, 1) {}
    Public Nb_Players As Integer = 2
    Public game As Partie

    Private Sub Config_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Continue_Button.Enabled = False
    End Sub

    Private Sub Check()

        If Input_P3.Enabled = True And Count_Color() = Count_Shape() Then
            Dim unique As Boolean
            unique = (Input_P1.Text <> Input_P2.Text) And (Input_P1.Text <> Input_P3.Text) And (Input_P2.Text <> Input_P3.Text) And (Input_P2.Text <> Input_P4.Text) And (Input_P1.Text <> Input_P4.Text)
            unique = unique And (Input_P3.Text <> Input_P4.Text Or Input_P3.Text = "")
            If unique Then
                Continue_Button.Enabled = True
            Else
                Continue_Button.Enabled = False
            End If
        Else
            Continue_Button.Enabled = False
        End If
    End Sub

    Public Function Check_P(n_player As Integer) As Boolean
        Check()
        If Input_P1.Text = "" Or Input_P2.Text = "" Then
            Return False
        End If

        If n_player = 4 Then
            If Input_P3.Text = "" Then
                Return False
            End If
        End If
        Check()
        Return True
    End Function

    Private Sub Cancel_Button_Click(sender As Object, e As EventArgs) Handles Cancel_Button.Click
        Dim Menu As New Menu

        Menu.TopLevel = False
        Menu.Dock = DockStyle.Fill
        Menu.FormBorderStyle = FormBorderStyle.None
        Me.Parent.Controls.Add(Menu)
        Menu.Show()
        Me.Hide()
        Me.Parent.Controls.Remove(Me)
    End Sub

    Private Sub Input_P1_P2_TextChanged(sender As Object, e As EventArgs) Handles Input_P1.TextChanged, Input_P2.TextChanged
        If Check_P(3) Then
            Input_P3.Enabled = True
            Date_In_P3.Enabled = True
        Else
            Input_P3.Text = ""
            Input_P3.Enabled = False
            Date_In_P3.Enabled = False
        End If
        Check()
    End Sub

    Private Sub Input_P3_TextChanged(sender As Object, e As EventArgs) Handles Input_P3.TextChanged
        If Check_P(4) Then
            Input_P4.Enabled = True
            Date_In_P4.Enabled = True
            Check()
        Else
            Input_P4.Text = ""
            Input_P4.Enabled = False
            Date_In_P4.Enabled = False
        End If
    End Sub

    Private Sub Input_P4_TextChanged(sender As Object, e As EventArgs) Handles Input_P4.TextChanged
        Check()
    End Sub

    Public Function Count_Shape() As Integer
        Dim count = 0
        Dim checkbox As New CheckBox

        For number = 1 To 9
            checkbox = Input_Token.Controls("Shape_In_" & number)
            If checkbox Is Nothing Then Return 0
            If checkbox.Checked Then
                count += 1
            End If
        Next

        Return count
    End Function

    Public Function Count_Color() As Integer
        Dim count = 0
        Dim checkbox As New CheckBox

        For number = 1 To 9
            checkbox = Input_Token.Controls("Color_In_" & number)
            If checkbox Is Nothing Then Return 0
            If checkbox.Checked Then
                count += 1
            End If
        Next

        Return count
    End Function

    Public Sub Test_Shape(Nb As Integer)
        Dim checkbox As CheckBox

        If Count_Shape() = 4 Then
            checkbox = Input_Token.Controls("Shape_In_" & Nb)
            If checkbox Is Nothing Then Exit Sub
            If checkbox.Checked = False Then
                checkbox.Checked = True
            End If
        End If

        Check()
    End Sub

    Private Sub Shape_In_1_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_1.CheckedChanged
        Test_Shape(1)
    End Sub
    Private Sub Shape_In_2_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_2.CheckedChanged
        Test_Shape(2)
    End Sub
    Private Sub Shape_In_3_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_3.CheckedChanged
        Test_Shape(3)
    End Sub
    Private Sub Shape_In_4_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_4.CheckedChanged
        Test_Shape(4)
    End Sub
    Private Sub Shape_In_5_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_5.CheckedChanged
        Test_Shape(5)
    End Sub
    Private Sub Shape_In_6_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_6.CheckedChanged
        Test_Shape(6)
    End Sub
    Private Sub Shape_In_7_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_7.CheckedChanged
        Test_Shape(7)
    End Sub
    Private Sub Shape_In_8_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_8.CheckedChanged
        Test_Shape(8)
    End Sub
    Private Sub Shape_In_9_CheckedChanged(sender As Object, e As EventArgs) Handles Shape_In_9.CheckedChanged
        Test_Shape(9)
    End Sub

    Public Sub Test_Color(Nb As Integer)
        Dim checkbox As CheckBox

        If Count_Color() = 4 Then
            checkbox = Input_Token.Controls("Color_In_" & Nb)
            If checkbox Is Nothing Then Exit Sub
            If checkbox.Checked = False Then
                checkbox.Checked = True
            End If
        End If

        Check()
    End Sub

    Private Sub Color_In_1_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_1.CheckedChanged
        Test_Color(1)
    End Sub
    Private Sub Color_In_2_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_2.CheckedChanged
        Test_Color(2)
    End Sub
    Private Sub Color_In_3_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_3.CheckedChanged
        Test_Color(3)
    End Sub
    Private Sub Color_In_4_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_4.CheckedChanged
        Test_Color(4)
    End Sub
    Private Sub Color_In_5_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_5.CheckedChanged
        Test_Color(5)
    End Sub
    Private Sub Color_In_6_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_6.CheckedChanged
        Test_Color(6)
    End Sub
    Private Sub Color_In_7_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_7.CheckedChanged
        Test_Color(7)
    End Sub
    Private Sub Color_In_8_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_8.CheckedChanged
        Test_Color(8)
    End Sub
    Private Sub Color_In_9_CheckedChanged(sender As Object, e As EventArgs) Handles Color_In_9.CheckedChanged
        Test_Color(9)
    End Sub

    Private Sub Continue_Button_Click(sender As Object, e As EventArgs) Handles Continue_Button.Click
        Continue_Button.Enabled = False
        Cancel_Button.Enabled = False   ' User can't Quit During the Game's Load

        ' Initializing Game with Parameters

        If Input_P4.Enabled Then        ' If P4's TextBox is Unlocked, we have a 3rd Player
            Nb_Players = 3
            If Input_P4.Text <> "" Then ' If it's not Empty, he Have a 4th
                Nb_Players = 4
            End If
        End If

        For iteration = 1 To 9          ' For Each Shape CheckBox,
            Dim checkbox As CheckBox    ' We get Its State
            checkbox = Input_Token.Controls("Shape_In_" & iteration)
            If checkbox.Checked Then    ' And Fill the first Half of the Token Table
                Token(iteration - 1, 0) = 1
            Else
                Token(iteration - 1, 0) = 0
            End If
        Next

        For iteration = 1 To 9          ' For Each Color CheckBox,
            Dim checkbox As CheckBox    ' We get Its State
            checkbox = Input_Token.Controls("Color_In_" & iteration)
            If checkbox.Checked Then    ' And Fill the other Half of the Token Table
                Token(iteration - 1, 1) = 1
            Else
                Token(iteration - 1, 1) = 0
            End If
        Next

        Dim players() As joueur = New joueur(Nb_Players - 1) {}

        For n_player = 1 To Nb_Players
            Dim name = Input_Name.Controls("Input_P" & n_player).Text
            If name Is Nothing Then Exit Sub

            Dim birthdate As New DateTimePicker
            birthdate = Input_Name.Controls("Date_In_P" & n_player)

            Dim day = birthdate.Value.Day
            Dim month = birthdate.Value.Month
            Dim year = birthdate.Value.Year

            players(n_player - 1) = New joueur(name, day, month, year)
        Next

        If rules.Visible = True Then
            rules.Hide()
        End If
        game = New Partie(Nb_Players, Token, players)    ' Now we Initialize the Game with its Data

        Dim Jeu As New Game         ' New Form
        Jeu.set_game(game)          ' The Form Needs the actual game var
        game = Nothing              ' We delete Local Game to Free some Space
        'Call Jeu.Game_Load(Nothing, Nothing)
        Jeu.TopLevel = False
        Jeu.Dock = DockStyle.Fill
        Jeu.FormBorderStyle = FormBorderStyle.None
        Me.Parent.Controls.Add(Jeu) ' We Display the Form
        Jeu.Show()
        Me.Close()                   'And Mask This One
    End Sub

    Private Sub Options_Click(sender As Object, e As EventArgs) Handles Options.Click
        If rules.Visible = True Then
            rules.Hide()
        Else
            rules.Show()
        End If
    End Sub
End Class