﻿Imports Projet_qwirkle

Public Class MainWindow

    Private Sub App_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Menu As New Menu
        Menu.TopLevel = False
        MainPanel.Controls.Add(Menu)
        Menu.Dock = DockStyle.Fill
        Menu.FormBorderStyle = FormBorderStyle.None
        Menu.Show()
    End Sub

End Class