﻿Imports Projet_qwirkle

Public Class Game
    Private rules As New Options

    Private game

    ' Setter
    Public Sub Set_Game(partie As Partie)
        game = partie
    End Sub

    ' Getter
    Public Function Get_Game() As Partie
        Return game
    End Function

    Private Sub Options_Click(sender As Object, e As EventArgs) Handles Options.Click
        If rules.Visible = True Then
            rules.Hide()
        Else
            rules.Show()
        End If
    End Sub

    Private Sub RefreshLabels()
        ' Update of the Deck
        game.get_class_pioche().intégrité_pioche()

        ' Data Labels Update
        Count_Deck.Text = ("Pioche: " & game.get_class_pioche().get_nbr_pions())
        Count_Turn.Text = ("Tour " & game.get_tour() + 1)
        P_Turn.Text = ("Tour de " & game.get_pseudo(game.get_qui_joue_int()))
        P_Hand.Text = game.get_pseudo(game.get_qui_joue_int())

        ' We Display the Player's Hand
        For slot = 1 To 6
            Dim token As New Label
            token = P_Hand.Controls("Hand_Slot_" & slot)
            If token Is Nothing Then Exit Sub
            token.AllowDrop = True
            token.Text = game.get_qui_joue_main().get_main()(slot - 1).Get_forme_string()
            token.BackColor = game.get_qui_joue_main().get_main()(slot - 1).Get_couleur_colo()
        Next

        ' We Empty the Slots for Token Reroll
        For slot = 1 To 6
            Dim token As New Label
            token = Reroll.Controls("Reroll_Slot_" & slot)
            If token Is Nothing Then Exit Sub
            token.AllowDrop = True
            token.Text = ""
            Dim white As New Color
            white = white
            token.BackColor = white
        Next

        ' Displays the Updated Score
        For player = 1 To game.get_nbr_joueur()
            Dim p_score As New TableLayoutPanel
            p_score = ScoreBoard.Controls("Stats_P" & player).Controls("Score_P" & player)
            If p_score Is Nothing Then Exit Sub

            p_score.Controls("Total_Score_P" & player).Text = game.get_player(player).get_score()
            p_score.Controls("Last_Score_P" & player).Text = game.get_player(player).get_score_dernier_tour()
        Next
    End Sub

    Private Sub Game_Load(sender As Object, e As EventArgs) Handles Me.Load
        BoardUpdate()

        For player = 1 To 4
            Dim stats_pj As New TableLayoutPanel
            stats_pj = ScoreBoard.Controls("Stats_P" & player)
            If stats_pj Is Nothing Then Exit Sub

            If player <= game.get_nbr_joueur() Then
                stats_pj.Controls("Name_P" & player).Text = game.get_player(player).get_player().get_pseudo()
            Else
                stats_pj.Hide()
            End If
        Next

        RefreshLabels()

    End Sub

    Private Sub Reroll_Button_Click(sender As Object, e As EventArgs) Handles Reroll_Button.Click
        game.fin_tour()
        Lock()
        RefreshLabels()
        BoardUpdate()
    End Sub

    Private Sub End_Turn_Button_Click(sender As Object, e As EventArgs) Handles End_Turn_Button.Click
        game.fin_tour()
        Lock()
        RefreshLabels()
        BoardUpdate()
    End Sub

    Public Sub BoardUpdate()
        Dim token_lbl As New Label
        Dim token As New pion
        For col = 0 To game.get_class_plateau().get_dimension()(0)
            For row = 0 To game.get_class_plateau().get_dimension()(0)
                If token_lbl Is Nothing Then Exit Sub
                ' token_lbl.Text = game.plateau.get_plateau()(col)(row).get_forme_string()
                ' token_lbl.BackColor = game.plateau.get_plateau()(col)(row).get_couleur_colo()
                ' token_lbl.AllowDrop = False
                ' GameBoard.Controls.Add(token_lbl)

                ' token.set_forme(game.plateau.get_plateau()(col)(row).get_forme())
                ' token.set_couleur(game.plateau.get_plateau()(col)(row).get_couleur())
            Next
        Next
    End Sub

    Public Sub Lock() ' Not Tested Yet
        Dim game_board As New TableLayoutPanel
        game_board = GameBoard
        If game_board Is Nothing Then Exit Sub
        Dim token As New pion
        Dim token_label As Label

        For row = 0 To game_board.RowCount
            For column = 0 To game_board.ColumnCount
                token = game.get_class_plateau().get_plateau_redimensionne()(column, row)
                Dim coordinates As New Point(column, row)
                token_label = game_board.GetChildAtPoint(coordinates)
                If token_label Is Nothing Then Exit Sub
                If token.get_lock() Or token.get_forme() = 0 Or token.get_couleur() = 0 Then
                    token_label.AllowDrop = False
                Else
                    token_label.AllowDrop = True
                    AddHandler token_label.MouseMove, AddressOf MM
                    AddHandler token_label.DragEnter, AddressOf DE
                    AddHandler token_label.DragDrop, AddressOf DD


                End If
            Next
        Next
    End Sub


    '-----------------------------------------------------------------------------------------------
    '                           Drag and Drop Methods
    '-----------------------------------------------------------------------------------------------

    ' MouseMove
    Public Sub MM(sender As Label, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            sender.DoDragDrop(sender, DragDropEffects.Move)
        End If
    End Sub

    ' DragEnter
    Private Sub DE(sender As Object, e As DragEventArgs)
        e.Effect = DragDropEffects.Move
    End Sub

    ' DragDrop
    Private Sub DD(sender As Label, e As DragEventArgs)
        Dim lbl As Label
        lbl = e.Data.GetData(sender.GetType().ToString)
        Dim token As New pion
        token.set_couleur_colo(lbl.BackColor)
        token.set_forme_string(lbl.Text)
        If sender.Text = "" Then
            sender.Text = lbl.Text
            sender.BackColor = lbl.BackColor
        End If
    End Sub


    ' MouseMove for Board
    Public Sub MM_Board(sender As Label, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            sender.DoDragDrop(sender, DragDropEffects.Move)
        End If
    End Sub

    ' DragEnter for Board
    Private Sub DE_Board(sender As Object, e As DragEventArgs)
        e.Effect = DragDropEffects.Move
    End Sub

    ' DragDrop for Board
    Private Sub DD_Board(sender As Label, e As DragEventArgs)
        Dim lbl As Label
        lbl = e.Data.GetData(sender.GetType().ToString)
        Dim token As New pion
        token.set_couleur_colo(lbl.BackColor)
        token.set_forme_string(lbl.Text)
        If game.plateau.test_placement(GameBoard.GetCellPosition(lbl).Column, GameBoard.GetCellPosition(lbl).Row, token) Then
            sender.Text = lbl.Text
            sender.BackColor = lbl.BackColor
            lbl.Text = ""
            Dim white As Color
            white = white
            lbl.BackColor = white
        End If
    End Sub


End Class