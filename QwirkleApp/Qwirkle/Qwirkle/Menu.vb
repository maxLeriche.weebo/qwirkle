﻿Public Class Menu
    Private rules As New Options

    Private Sub Options_Button_Click(sender As Object, e As EventArgs) Handles Options_Button.Click
        If rules.Visible = True Then
            rules.Hide()
        Else
            rules.Show()
        End If
    End Sub

    Private Sub Play_Button_Click(sender As Object, e As EventArgs) Handles Play_Button.Click
        Dim GameConfig As New Config

        GameConfig.TopLevel = False
        GameConfig.Dock = DockStyle.Fill
        GameConfig.FormBorderStyle = FormBorderStyle.None
        Me.Parent.Controls.Add(GameConfig)
        GameConfig.Show()
        Parent.Controls.Remove(Me)

        If rules.Visible = True Then
            rules.Hide()
        End If
    End Sub

    Private Sub App_Quit_Button_Click(sender As Object, e As EventArgs) Handles App_Quit_Button.Click
        MainWindow.Close()
    End Sub
End Class