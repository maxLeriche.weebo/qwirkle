﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Game
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Game))
        Me.Main = New System.Windows.Forms.TableLayoutPanel()
        Me.ScoreBoard = New System.Windows.Forms.TableLayoutPanel()
        Me.Stats_P4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Name_P4 = New System.Windows.Forms.Label()
        Me.Score_P4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Last_Score_P4 = New System.Windows.Forms.Label()
        Me.Total_Score_P4 = New System.Windows.Forms.Label()
        Me.Stats_P3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Name_P3 = New System.Windows.Forms.Label()
        Me.Score_P3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Last_Score_P3 = New System.Windows.Forms.Label()
        Me.Total_Score_P3 = New System.Windows.Forms.Label()
        Me.Stats_P2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Name_P2 = New System.Windows.Forms.Label()
        Me.Score_P2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Last_Score_P2 = New System.Windows.Forms.Label()
        Me.Total_Score_P2 = New System.Windows.Forms.Label()
        Me.Stats_P1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Name_P1 = New System.Windows.Forms.Label()
        Me.Score_P1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Last_Score_P1 = New System.Windows.Forms.Label()
        Me.Total_Score_P1 = New System.Windows.Forms.Label()
        Me.Options = New System.Windows.Forms.Button()
        Me.Count_Deck = New System.Windows.Forms.Label()
        Me.Count_Turn = New System.Windows.Forms.Label()
        Me.P_Turn = New System.Windows.Forms.Label()
        Me.Reroll = New System.Windows.Forms.GroupBox()
        Me.Reroll_Slots = New System.Windows.Forms.TableLayoutPanel()
        Me.Reroll_Slot_6 = New System.Windows.Forms.Label()
        Me.Reroll_Slot_5 = New System.Windows.Forms.Label()
        Me.Reroll_Slot_4 = New System.Windows.Forms.Label()
        Me.Reroll_Slot_3 = New System.Windows.Forms.Label()
        Me.Reroll_Slot_2 = New System.Windows.Forms.Label()
        Me.Reroll_Slot_1 = New System.Windows.Forms.Label()
        Me.P_Hand = New System.Windows.Forms.GroupBox()
        Me.Hand_Slots = New System.Windows.Forms.TableLayoutPanel()
        Me.Hand_Slot_6 = New System.Windows.Forms.Label()
        Me.Hand_Slot_5 = New System.Windows.Forms.Label()
        Me.Hand_Slot_4 = New System.Windows.Forms.Label()
        Me.Hand_Slot_3 = New System.Windows.Forms.Label()
        Me.Hand_Slot_2 = New System.Windows.Forms.Label()
        Me.Hand_Slot_1 = New System.Windows.Forms.Label()
        Me.Reroll_Button = New System.Windows.Forms.Button()
        Me.End_Turn_Button = New System.Windows.Forms.Button()
        Me.Game_Quit_Button = New System.Windows.Forms.Button()
        Me.GameBoard = New System.Windows.Forms.TableLayoutPanel()
        Me.Main.SuspendLayout()
        Me.ScoreBoard.SuspendLayout()
        Me.Stats_P4.SuspendLayout()
        Me.Score_P4.SuspendLayout()
        Me.Stats_P3.SuspendLayout()
        Me.Score_P3.SuspendLayout()
        Me.Stats_P2.SuspendLayout()
        Me.Score_P2.SuspendLayout()
        Me.Stats_P1.SuspendLayout()
        Me.Score_P1.SuspendLayout()
        Me.Reroll.SuspendLayout()
        Me.Reroll_Slots.SuspendLayout()
        Me.P_Hand.SuspendLayout()
        Me.Hand_Slots.SuspendLayout()
        Me.SuspendLayout()
        '
        'Main
        '
        Me.Main.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Main.ColumnCount = 2
        Me.Main.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.Main.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.0!))
        Me.Main.Controls.Add(Me.GameBoard, 0, 0)
        Me.Main.Controls.Add(Me.ScoreBoard, 0, 0)
        Me.Main.Location = New System.Drawing.Point(10, 45)
        Me.Main.Margin = New System.Windows.Forms.Padding(0)
        Me.Main.Name = "Main"
        Me.Main.RowCount = 1
        Me.Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Main.Size = New System.Drawing.Size(780, 312)
        Me.Main.TabIndex = 0
        '
        'ScoreBoard
        '
        Me.ScoreBoard.ColumnCount = 1
        Me.ScoreBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.ScoreBoard.Controls.Add(Me.Stats_P4, 0, 3)
        Me.ScoreBoard.Controls.Add(Me.Stats_P3, 0, 2)
        Me.ScoreBoard.Controls.Add(Me.Stats_P2, 0, 1)
        Me.ScoreBoard.Controls.Add(Me.Stats_P1, 0, 0)
        Me.ScoreBoard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ScoreBoard.Location = New System.Drawing.Point(0, 0)
        Me.ScoreBoard.Margin = New System.Windows.Forms.Padding(0)
        Me.ScoreBoard.Name = "ScoreBoard"
        Me.ScoreBoard.RowCount = 4
        Me.ScoreBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.ScoreBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.ScoreBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.ScoreBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.ScoreBoard.Size = New System.Drawing.Size(156, 312)
        Me.ScoreBoard.TabIndex = 0
        '
        'Stats_P4
        '
        Me.Stats_P4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.Stats_P4.ColumnCount = 1
        Me.Stats_P4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Stats_P4.Controls.Add(Me.Name_P4, 0, 0)
        Me.Stats_P4.Controls.Add(Me.Score_P4, 0, 1)
        Me.Stats_P4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Stats_P4.Location = New System.Drawing.Point(5, 239)
        Me.Stats_P4.Margin = New System.Windows.Forms.Padding(5)
        Me.Stats_P4.Name = "Stats_P4"
        Me.Stats_P4.RowCount = 2
        Me.Stats_P4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.Stats_P4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.0!))
        Me.Stats_P4.Size = New System.Drawing.Size(146, 68)
        Me.Stats_P4.TabIndex = 15
        '
        'Name_P4
        '
        Me.Name_P4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Name_P4.AutoSize = True
        Me.Name_P4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name_P4.Location = New System.Drawing.Point(33, 1)
        Me.Name_P4.Margin = New System.Windows.Forms.Padding(0)
        Me.Name_P4.Name = "Name_P4"
        Me.Name_P4.Size = New System.Drawing.Size(79, 21)
        Me.Name_P4.TabIndex = 0
        Me.Name_P4.Text = "Joueur 4"
        '
        'Score_P4
        '
        Me.Score_P4.ColumnCount = 2
        Me.Score_P4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Score_P4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Score_P4.Controls.Add(Me.Last_Score_P4, 0, 0)
        Me.Score_P4.Controls.Add(Me.Total_Score_P4, 0, 0)
        Me.Score_P4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Score_P4.Location = New System.Drawing.Point(4, 26)
        Me.Score_P4.Name = "Score_P4"
        Me.Score_P4.RowCount = 1
        Me.Score_P4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Score_P4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.Score_P4.Size = New System.Drawing.Size(138, 38)
        Me.Score_P4.TabIndex = 1
        '
        'Last_Score_P4
        '
        Me.Last_Score_P4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Last_Score_P4.AutoSize = True
        Me.Last_Score_P4.Location = New System.Drawing.Point(89, 12)
        Me.Last_Score_P4.Margin = New System.Windows.Forms.Padding(20, 0, 3, 0)
        Me.Last_Score_P4.Name = "Last_Score_P4"
        Me.Last_Score_P4.Size = New System.Drawing.Size(46, 13)
        Me.Last_Score_P4.TabIndex = 2
        Me.Last_Score_P4.Text = "(+0)"
        '
        'Total_Score_P4
        '
        Me.Total_Score_P4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Total_Score_P4.AutoSize = True
        Me.Total_Score_P4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total_Score_P4.Location = New System.Drawing.Point(21, 0)
        Me.Total_Score_P4.Margin = New System.Windows.Forms.Padding(0)
        Me.Total_Score_P4.Name = "Total_Score_P4"
        Me.Total_Score_P4.Size = New System.Drawing.Size(27, 38)
        Me.Total_Score_P4.TabIndex = 1
        Me.Total_Score_P4.Text = "0"
        '
        'Stats_P3
        '
        Me.Stats_P3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.Stats_P3.ColumnCount = 1
        Me.Stats_P3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Stats_P3.Controls.Add(Me.Name_P3, 0, 0)
        Me.Stats_P3.Controls.Add(Me.Score_P3, 0, 1)
        Me.Stats_P3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Stats_P3.Location = New System.Drawing.Point(5, 161)
        Me.Stats_P3.Margin = New System.Windows.Forms.Padding(5)
        Me.Stats_P3.Name = "Stats_P3"
        Me.Stats_P3.RowCount = 2
        Me.Stats_P3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.Stats_P3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.0!))
        Me.Stats_P3.Size = New System.Drawing.Size(146, 68)
        Me.Stats_P3.TabIndex = 14
        '
        'Name_P3
        '
        Me.Name_P3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Name_P3.AutoSize = True
        Me.Name_P3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name_P3.Location = New System.Drawing.Point(33, 1)
        Me.Name_P3.Margin = New System.Windows.Forms.Padding(0)
        Me.Name_P3.Name = "Name_P3"
        Me.Name_P3.Size = New System.Drawing.Size(79, 21)
        Me.Name_P3.TabIndex = 0
        Me.Name_P3.Text = "Joueur 3"
        '
        'Score_P3
        '
        Me.Score_P3.ColumnCount = 2
        Me.Score_P3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Score_P3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Score_P3.Controls.Add(Me.Last_Score_P3, 0, 0)
        Me.Score_P3.Controls.Add(Me.Total_Score_P3, 0, 0)
        Me.Score_P3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Score_P3.Location = New System.Drawing.Point(4, 26)
        Me.Score_P3.Name = "Score_P3"
        Me.Score_P3.RowCount = 1
        Me.Score_P3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Score_P3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.Score_P3.Size = New System.Drawing.Size(138, 38)
        Me.Score_P3.TabIndex = 1
        '
        'Last_Score_P3
        '
        Me.Last_Score_P3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Last_Score_P3.AutoSize = True
        Me.Last_Score_P3.Location = New System.Drawing.Point(89, 12)
        Me.Last_Score_P3.Margin = New System.Windows.Forms.Padding(20, 0, 3, 0)
        Me.Last_Score_P3.Name = "Last_Score_P3"
        Me.Last_Score_P3.Size = New System.Drawing.Size(46, 13)
        Me.Last_Score_P3.TabIndex = 2
        Me.Last_Score_P3.Text = "(+0)"
        '
        'Total_Score_P3
        '
        Me.Total_Score_P3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Total_Score_P3.AutoSize = True
        Me.Total_Score_P3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total_Score_P3.Location = New System.Drawing.Point(21, 0)
        Me.Total_Score_P3.Margin = New System.Windows.Forms.Padding(0)
        Me.Total_Score_P3.Name = "Total_Score_P3"
        Me.Total_Score_P3.Size = New System.Drawing.Size(27, 38)
        Me.Total_Score_P3.TabIndex = 1
        Me.Total_Score_P3.Text = "0"
        '
        'Stats_P2
        '
        Me.Stats_P2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.Stats_P2.ColumnCount = 1
        Me.Stats_P2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Stats_P2.Controls.Add(Me.Name_P2, 0, 0)
        Me.Stats_P2.Controls.Add(Me.Score_P2, 0, 1)
        Me.Stats_P2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Stats_P2.Location = New System.Drawing.Point(5, 83)
        Me.Stats_P2.Margin = New System.Windows.Forms.Padding(5)
        Me.Stats_P2.Name = "Stats_P2"
        Me.Stats_P2.RowCount = 2
        Me.Stats_P2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.Stats_P2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.0!))
        Me.Stats_P2.Size = New System.Drawing.Size(146, 68)
        Me.Stats_P2.TabIndex = 13
        '
        'Name_P2
        '
        Me.Name_P2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Name_P2.AutoSize = True
        Me.Name_P2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name_P2.Location = New System.Drawing.Point(33, 1)
        Me.Name_P2.Margin = New System.Windows.Forms.Padding(0)
        Me.Name_P2.Name = "Name_P2"
        Me.Name_P2.Size = New System.Drawing.Size(79, 21)
        Me.Name_P2.TabIndex = 0
        Me.Name_P2.Text = "Joueur 2"
        '
        'Score_P2
        '
        Me.Score_P2.ColumnCount = 2
        Me.Score_P2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Score_P2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Score_P2.Controls.Add(Me.Last_Score_P2, 0, 0)
        Me.Score_P2.Controls.Add(Me.Total_Score_P2, 0, 0)
        Me.Score_P2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Score_P2.Location = New System.Drawing.Point(4, 26)
        Me.Score_P2.Name = "Score_P2"
        Me.Score_P2.RowCount = 1
        Me.Score_P2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Score_P2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.Score_P2.Size = New System.Drawing.Size(138, 38)
        Me.Score_P2.TabIndex = 1
        '
        'Last_Score_P2
        '
        Me.Last_Score_P2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Last_Score_P2.AutoSize = True
        Me.Last_Score_P2.Location = New System.Drawing.Point(89, 12)
        Me.Last_Score_P2.Margin = New System.Windows.Forms.Padding(20, 0, 3, 0)
        Me.Last_Score_P2.Name = "Last_Score_P2"
        Me.Last_Score_P2.Size = New System.Drawing.Size(46, 13)
        Me.Last_Score_P2.TabIndex = 2
        Me.Last_Score_P2.Text = "(+0)"
        '
        'Total_Score_P2
        '
        Me.Total_Score_P2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Total_Score_P2.AutoSize = True
        Me.Total_Score_P2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total_Score_P2.Location = New System.Drawing.Point(21, 0)
        Me.Total_Score_P2.Margin = New System.Windows.Forms.Padding(0)
        Me.Total_Score_P2.Name = "Total_Score_P2"
        Me.Total_Score_P2.Size = New System.Drawing.Size(27, 38)
        Me.Total_Score_P2.TabIndex = 1
        Me.Total_Score_P2.Text = "0"
        '
        'Stats_P1
        '
        Me.Stats_P1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.Stats_P1.ColumnCount = 1
        Me.Stats_P1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Stats_P1.Controls.Add(Me.Name_P1, 0, 0)
        Me.Stats_P1.Controls.Add(Me.Score_P1, 0, 1)
        Me.Stats_P1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Stats_P1.Location = New System.Drawing.Point(5, 5)
        Me.Stats_P1.Margin = New System.Windows.Forms.Padding(5)
        Me.Stats_P1.Name = "Stats_P1"
        Me.Stats_P1.RowCount = 2
        Me.Stats_P1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.Stats_P1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.0!))
        Me.Stats_P1.Size = New System.Drawing.Size(146, 68)
        Me.Stats_P1.TabIndex = 12
        '
        'Name_P1
        '
        Me.Name_P1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Name_P1.AutoSize = True
        Me.Name_P1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name_P1.Location = New System.Drawing.Point(33, 1)
        Me.Name_P1.Margin = New System.Windows.Forms.Padding(0)
        Me.Name_P1.Name = "Name_P1"
        Me.Name_P1.Size = New System.Drawing.Size(79, 21)
        Me.Name_P1.TabIndex = 0
        Me.Name_P1.Text = "Joueur 1"
        '
        'Score_P1
        '
        Me.Score_P1.ColumnCount = 2
        Me.Score_P1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Score_P1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Score_P1.Controls.Add(Me.Last_Score_P1, 0, 0)
        Me.Score_P1.Controls.Add(Me.Total_Score_P1, 0, 0)
        Me.Score_P1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Score_P1.Location = New System.Drawing.Point(4, 26)
        Me.Score_P1.Name = "Score_P1"
        Me.Score_P1.RowCount = 1
        Me.Score_P1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Score_P1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.Score_P1.Size = New System.Drawing.Size(138, 38)
        Me.Score_P1.TabIndex = 1
        '
        'Last_Score_P1
        '
        Me.Last_Score_P1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Last_Score_P1.AutoSize = True
        Me.Last_Score_P1.Location = New System.Drawing.Point(89, 12)
        Me.Last_Score_P1.Margin = New System.Windows.Forms.Padding(20, 0, 3, 0)
        Me.Last_Score_P1.Name = "Last_Score_P1"
        Me.Last_Score_P1.Size = New System.Drawing.Size(46, 13)
        Me.Last_Score_P1.TabIndex = 2
        Me.Last_Score_P1.Text = "(+0)"
        '
        'Total_Score_P1
        '
        Me.Total_Score_P1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Total_Score_P1.AutoSize = True
        Me.Total_Score_P1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total_Score_P1.Location = New System.Drawing.Point(21, 0)
        Me.Total_Score_P1.Margin = New System.Windows.Forms.Padding(0)
        Me.Total_Score_P1.Name = "Total_Score_P1"
        Me.Total_Score_P1.Size = New System.Drawing.Size(27, 38)
        Me.Total_Score_P1.TabIndex = 1
        Me.Total_Score_P1.Text = "0"
        '
        'Options
        '
        Me.Options.BackgroundImage = CType(resources.GetObject("Options.BackgroundImage"), System.Drawing.Image)
        Me.Options.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Options.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Options.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.Options.FlatAppearance.BorderSize = 0
        Me.Options.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Options.Location = New System.Drawing.Point(10, 10)
        Me.Options.Margin = New System.Windows.Forms.Padding(0)
        Me.Options.Name = "Options"
        Me.Options.Size = New System.Drawing.Size(24, 24)
        Me.Options.TabIndex = 13
        Me.Options.UseMnemonic = False
        Me.Options.UseVisualStyleBackColor = False
        Me.Options.UseWaitCursor = True
        '
        'Count_Deck
        '
        Me.Count_Deck.AutoSize = True
        Me.Count_Deck.Location = New System.Drawing.Point(170, 20)
        Me.Count_Deck.Name = "Count_Deck"
        Me.Count_Deck.Size = New System.Drawing.Size(46, 13)
        Me.Count_Deck.TabIndex = 15
        Me.Count_Deck.Text = "Pioche: "
        '
        'Count_Turn
        '
        Me.Count_Turn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Count_Turn.AutoSize = True
        Me.Count_Turn.Location = New System.Drawing.Point(745, 20)
        Me.Count_Turn.Margin = New System.Windows.Forms.Padding(3, 0, 10, 0)
        Me.Count_Turn.Name = "Count_Turn"
        Me.Count_Turn.Size = New System.Drawing.Size(38, 13)
        Me.Count_Turn.TabIndex = 14
        Me.Count_Turn.Text = "Tour 0"
        '
        'P_Turn
        '
        Me.P_Turn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Turn.AutoSize = True
        Me.P_Turn.Location = New System.Drawing.Point(450, 20)
        Me.P_Turn.Name = "P_Turn"
        Me.P_Turn.Size = New System.Drawing.Size(47, 13)
        Me.P_Turn.TabIndex = 16
        Me.P_Turn.Text = "Tour de "
        '
        'Reroll
        '
        Me.Reroll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Reroll.Controls.Add(Me.Reroll_Slots)
        Me.Reroll.Location = New System.Drawing.Point(247, 375)
        Me.Reroll.Name = "Reroll"
        Me.Reroll.Padding = New System.Windows.Forms.Padding(0)
        Me.Reroll.Size = New System.Drawing.Size(220, 64)
        Me.Reroll.TabIndex = 18
        Me.Reroll.TabStop = False
        Me.Reroll.Text = "Remplacer"
        '
        'Reroll_Slots
        '
        Me.Reroll_Slots.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.Reroll_Slots.ColumnCount = 6
        Me.Reroll_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Reroll_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Reroll_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Reroll_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Reroll_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Reroll_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.Reroll_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Reroll_Slots.Controls.Add(Me.Reroll_Slot_6, 5, 0)
        Me.Reroll_Slots.Controls.Add(Me.Reroll_Slot_5, 4, 0)
        Me.Reroll_Slots.Controls.Add(Me.Reroll_Slot_4, 3, 0)
        Me.Reroll_Slots.Controls.Add(Me.Reroll_Slot_3, 2, 0)
        Me.Reroll_Slots.Controls.Add(Me.Reroll_Slot_2, 1, 0)
        Me.Reroll_Slots.Controls.Add(Me.Reroll_Slot_1, 0, 0)
        Me.Reroll_Slots.Location = New System.Drawing.Point(10, 20)
        Me.Reroll_Slots.Margin = New System.Windows.Forms.Padding(0)
        Me.Reroll_Slots.Name = "Reroll_Slots"
        Me.Reroll_Slots.RowCount = 1
        Me.Reroll_Slots.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Reroll_Slots.Size = New System.Drawing.Size(197, 32)
        Me.Reroll_Slots.TabIndex = 9
        '
        'Reroll_Slot_6
        '
        Me.Reroll_Slot_6.AutoSize = True
        Me.Reroll_Slot_6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Reroll_Slot_6.Location = New System.Drawing.Point(166, 1)
        Me.Reroll_Slot_6.Margin = New System.Windows.Forms.Padding(0)
        Me.Reroll_Slot_6.Name = "Reroll_Slot_6"
        Me.Reroll_Slot_6.Size = New System.Drawing.Size(43, 30)
        Me.Reroll_Slot_6.TabIndex = 10
        '
        'Reroll_Slot_5
        '
        Me.Reroll_Slot_5.AutoSize = True
        Me.Reroll_Slot_5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Reroll_Slot_5.Location = New System.Drawing.Point(133, 1)
        Me.Reroll_Slot_5.Margin = New System.Windows.Forms.Padding(0)
        Me.Reroll_Slot_5.Name = "Reroll_Slot_5"
        Me.Reroll_Slot_5.Size = New System.Drawing.Size(32, 30)
        Me.Reroll_Slot_5.TabIndex = 9
        '
        'Reroll_Slot_4
        '
        Me.Reroll_Slot_4.AutoSize = True
        Me.Reroll_Slot_4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Reroll_Slot_4.Location = New System.Drawing.Point(100, 1)
        Me.Reroll_Slot_4.Margin = New System.Windows.Forms.Padding(0)
        Me.Reroll_Slot_4.Name = "Reroll_Slot_4"
        Me.Reroll_Slot_4.Size = New System.Drawing.Size(32, 30)
        Me.Reroll_Slot_4.TabIndex = 8
        '
        'Reroll_Slot_3
        '
        Me.Reroll_Slot_3.AutoSize = True
        Me.Reroll_Slot_3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Reroll_Slot_3.Location = New System.Drawing.Point(67, 1)
        Me.Reroll_Slot_3.Margin = New System.Windows.Forms.Padding(0)
        Me.Reroll_Slot_3.Name = "Reroll_Slot_3"
        Me.Reroll_Slot_3.Size = New System.Drawing.Size(32, 30)
        Me.Reroll_Slot_3.TabIndex = 7
        '
        'Reroll_Slot_2
        '
        Me.Reroll_Slot_2.AutoSize = True
        Me.Reroll_Slot_2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Reroll_Slot_2.Location = New System.Drawing.Point(34, 1)
        Me.Reroll_Slot_2.Margin = New System.Windows.Forms.Padding(0)
        Me.Reroll_Slot_2.Name = "Reroll_Slot_2"
        Me.Reroll_Slot_2.Size = New System.Drawing.Size(32, 30)
        Me.Reroll_Slot_2.TabIndex = 6
        '
        'Reroll_Slot_1
        '
        Me.Reroll_Slot_1.AutoSize = True
        Me.Reroll_Slot_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Reroll_Slot_1.Location = New System.Drawing.Point(1, 1)
        Me.Reroll_Slot_1.Margin = New System.Windows.Forms.Padding(0)
        Me.Reroll_Slot_1.Name = "Reroll_Slot_1"
        Me.Reroll_Slot_1.Size = New System.Drawing.Size(32, 30)
        Me.Reroll_Slot_1.TabIndex = 5
        '
        'P_Hand
        '
        Me.P_Hand.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.P_Hand.Controls.Add(Me.Hand_Slots)
        Me.P_Hand.Location = New System.Drawing.Point(10, 375)
        Me.P_Hand.Name = "P_Hand"
        Me.P_Hand.Padding = New System.Windows.Forms.Padding(0)
        Me.P_Hand.Size = New System.Drawing.Size(220, 64)
        Me.P_Hand.TabIndex = 17
        Me.P_Hand.TabStop = False
        Me.P_Hand.Text = "Vous"
        '
        'Hand_Slots
        '
        Me.Hand_Slots.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.Hand_Slots.ColumnCount = 6
        Me.Hand_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Hand_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Hand_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Hand_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Hand_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.Hand_Slots.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.Hand_Slots.Controls.Add(Me.Hand_Slot_6, 5, 0)
        Me.Hand_Slots.Controls.Add(Me.Hand_Slot_5, 4, 0)
        Me.Hand_Slots.Controls.Add(Me.Hand_Slot_4, 3, 0)
        Me.Hand_Slots.Controls.Add(Me.Hand_Slot_3, 2, 0)
        Me.Hand_Slots.Controls.Add(Me.Hand_Slot_2, 1, 0)
        Me.Hand_Slots.Controls.Add(Me.Hand_Slot_1, 0, 0)
        Me.Hand_Slots.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize
        Me.Hand_Slots.Location = New System.Drawing.Point(10, 20)
        Me.Hand_Slots.Margin = New System.Windows.Forms.Padding(0)
        Me.Hand_Slots.Name = "Hand_Slots"
        Me.Hand_Slots.RowCount = 1
        Me.Hand_Slots.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Hand_Slots.Size = New System.Drawing.Size(197, 32)
        Me.Hand_Slots.TabIndex = 9
        '
        'Hand_Slot_6
        '
        Me.Hand_Slot_6.AutoSize = True
        Me.Hand_Slot_6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Hand_Slot_6.Location = New System.Drawing.Point(166, 1)
        Me.Hand_Slot_6.Margin = New System.Windows.Forms.Padding(0)
        Me.Hand_Slot_6.Name = "Hand_Slot_6"
        Me.Hand_Slot_6.Size = New System.Drawing.Size(43, 30)
        Me.Hand_Slot_6.TabIndex = 5
        '
        'Hand_Slot_5
        '
        Me.Hand_Slot_5.AutoSize = True
        Me.Hand_Slot_5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Hand_Slot_5.Location = New System.Drawing.Point(133, 1)
        Me.Hand_Slot_5.Margin = New System.Windows.Forms.Padding(0)
        Me.Hand_Slot_5.Name = "Hand_Slot_5"
        Me.Hand_Slot_5.Size = New System.Drawing.Size(32, 30)
        Me.Hand_Slot_5.TabIndex = 4
        '
        'Hand_Slot_4
        '
        Me.Hand_Slot_4.AutoSize = True
        Me.Hand_Slot_4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Hand_Slot_4.Location = New System.Drawing.Point(100, 1)
        Me.Hand_Slot_4.Margin = New System.Windows.Forms.Padding(0)
        Me.Hand_Slot_4.Name = "Hand_Slot_4"
        Me.Hand_Slot_4.Size = New System.Drawing.Size(32, 30)
        Me.Hand_Slot_4.TabIndex = 3
        '
        'Hand_Slot_3
        '
        Me.Hand_Slot_3.AutoSize = True
        Me.Hand_Slot_3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Hand_Slot_3.Location = New System.Drawing.Point(67, 1)
        Me.Hand_Slot_3.Margin = New System.Windows.Forms.Padding(0)
        Me.Hand_Slot_3.Name = "Hand_Slot_3"
        Me.Hand_Slot_3.Size = New System.Drawing.Size(32, 30)
        Me.Hand_Slot_3.TabIndex = 2
        '
        'Hand_Slot_2
        '
        Me.Hand_Slot_2.AutoSize = True
        Me.Hand_Slot_2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Hand_Slot_2.Location = New System.Drawing.Point(34, 1)
        Me.Hand_Slot_2.Margin = New System.Windows.Forms.Padding(0)
        Me.Hand_Slot_2.Name = "Hand_Slot_2"
        Me.Hand_Slot_2.Size = New System.Drawing.Size(32, 30)
        Me.Hand_Slot_2.TabIndex = 1
        '
        'Hand_Slot_1
        '
        Me.Hand_Slot_1.AutoSize = True
        Me.Hand_Slot_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Hand_Slot_1.Location = New System.Drawing.Point(1, 1)
        Me.Hand_Slot_1.Margin = New System.Windows.Forms.Padding(0)
        Me.Hand_Slot_1.Name = "Hand_Slot_1"
        Me.Hand_Slot_1.Size = New System.Drawing.Size(32, 30)
        Me.Hand_Slot_1.TabIndex = 0
        '
        'Reroll_Button
        '
        Me.Reroll_Button.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Reroll_Button.Location = New System.Drawing.Point(460, 395)
        Me.Reroll_Button.Name = "Reroll_Button"
        Me.Reroll_Button.Size = New System.Drawing.Size(50, 32)
        Me.Reroll_Button.TabIndex = 19
        Me.Reroll_Button.Text = "Reroll"
        Me.Reroll_Button.UseVisualStyleBackColor = True
        '
        'End_Turn_Button
        '
        Me.End_Turn_Button.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.End_Turn_Button.Location = New System.Drawing.Point(704, 395)
        Me.End_Turn_Button.Name = "End_Turn_Button"
        Me.End_Turn_Button.Size = New System.Drawing.Size(90, 32)
        Me.End_Turn_Button.TabIndex = 20
        Me.End_Turn_Button.Text = "Fin du Tour"
        Me.End_Turn_Button.UseVisualStyleBackColor = True
        '
        'Game_Quit_Button
        '
        Me.Game_Quit_Button.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Game_Quit_Button.Location = New System.Drawing.Point(570, 395)
        Me.Game_Quit_Button.Name = "Game_Quit_Button"
        Me.Game_Quit_Button.Size = New System.Drawing.Size(80, 32)
        Me.Game_Quit_Button.TabIndex = 21
        Me.Game_Quit_Button.Text = "Abandonner"
        Me.Game_Quit_Button.UseVisualStyleBackColor = True
        '
        'GameBoard
        '
        Me.GameBoard.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset
        Me.GameBoard.ColumnCount = 10
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GameBoard.Location = New System.Drawing.Point(159, 3)
        Me.GameBoard.Name = "GameBoard"
        Me.GameBoard.RowCount = 10
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.GameBoard.Size = New System.Drawing.Size(618, 306)
        Me.GameBoard.TabIndex = 1
        '
        'Game
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Game_Quit_Button)
        Me.Controls.Add(Me.End_Turn_Button)
        Me.Controls.Add(Me.Reroll_Button)
        Me.Controls.Add(Me.Reroll)
        Me.Controls.Add(Me.P_Hand)
        Me.Controls.Add(Me.P_Turn)
        Me.Controls.Add(Me.Count_Deck)
        Me.Controls.Add(Me.Count_Turn)
        Me.Controls.Add(Me.Options)
        Me.Controls.Add(Me.Main)
        Me.Name = "Game"
        Me.Text = "Game"
        Me.Main.ResumeLayout(False)
        Me.ScoreBoard.ResumeLayout(False)
        Me.Stats_P4.ResumeLayout(False)
        Me.Stats_P4.PerformLayout()
        Me.Score_P4.ResumeLayout(False)
        Me.Score_P4.PerformLayout()
        Me.Stats_P3.ResumeLayout(False)
        Me.Stats_P3.PerformLayout()
        Me.Score_P3.ResumeLayout(False)
        Me.Score_P3.PerformLayout()
        Me.Stats_P2.ResumeLayout(False)
        Me.Stats_P2.PerformLayout()
        Me.Score_P2.ResumeLayout(False)
        Me.Score_P2.PerformLayout()
        Me.Stats_P1.ResumeLayout(False)
        Me.Stats_P1.PerformLayout()
        Me.Score_P1.ResumeLayout(False)
        Me.Score_P1.PerformLayout()
        Me.Reroll.ResumeLayout(False)
        Me.Reroll_Slots.ResumeLayout(False)
        Me.Reroll_Slots.PerformLayout()
        Me.P_Hand.ResumeLayout(False)
        Me.Hand_Slots.ResumeLayout(False)
        Me.Hand_Slots.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Main As TableLayoutPanel
    Friend WithEvents ScoreBoard As TableLayoutPanel
    Friend WithEvents Options As Button
    Friend WithEvents Count_Deck As Label
    Friend WithEvents Count_Turn As Label
    Friend WithEvents P_Turn As Label
    Friend WithEvents Reroll As GroupBox
    Friend WithEvents Reroll_Slots As TableLayoutPanel
    Friend WithEvents P_Hand As GroupBox
    Friend WithEvents Hand_Slots As TableLayoutPanel
    Friend WithEvents Reroll_Button As Button
    Friend WithEvents End_Turn_Button As Button
    Friend WithEvents Game_Quit_Button As Button
    Friend WithEvents Stats_P4 As TableLayoutPanel
    Friend WithEvents Name_P4 As Label
    Friend WithEvents Score_P4 As TableLayoutPanel
    Friend WithEvents Last_Score_P4 As Label
    Friend WithEvents Total_Score_P4 As Label
    Friend WithEvents Stats_P3 As TableLayoutPanel
    Friend WithEvents Name_P3 As Label
    Friend WithEvents Score_P3 As TableLayoutPanel
    Friend WithEvents Last_Score_P3 As Label
    Friend WithEvents Total_Score_P3 As Label
    Friend WithEvents Stats_P2 As TableLayoutPanel
    Friend WithEvents Name_P2 As Label
    Friend WithEvents Score_P2 As TableLayoutPanel
    Friend WithEvents Last_Score_P2 As Label
    Friend WithEvents Total_Score_P2 As Label
    Friend WithEvents Stats_P1 As TableLayoutPanel
    Friend WithEvents Name_P1 As Label
    Friend WithEvents Score_P1 As TableLayoutPanel
    Friend WithEvents Last_Score_P1 As Label
    Friend WithEvents Total_Score_P1 As Label
    Friend WithEvents Reroll_Slot_6 As Label
    Friend WithEvents Reroll_Slot_5 As Label
    Friend WithEvents Reroll_Slot_4 As Label
    Friend WithEvents Reroll_Slot_3 As Label
    Friend WithEvents Reroll_Slot_2 As Label
    Friend WithEvents Reroll_Slot_1 As Label
    Friend WithEvents Hand_Slot_6 As Label
    Friend WithEvents Hand_Slot_5 As Label
    Friend WithEvents Hand_Slot_4 As Label
    Friend WithEvents Hand_Slot_3 As Label
    Friend WithEvents Hand_Slot_2 As Label
    Friend WithEvents Hand_Slot_1 As Label
    Friend WithEvents GameBoard As TableLayoutPanel
End Class
