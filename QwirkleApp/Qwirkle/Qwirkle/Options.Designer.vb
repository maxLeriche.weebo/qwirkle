﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Options
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Return_Button = New System.Windows.Forms.Button()
        Me.Panel = New System.Windows.Forms.Panel()
        Me.SuspendLayout()
        '
        'Return_Button
        '
        Me.Return_Button.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Return_Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Return_Button.Location = New System.Drawing.Point(0, 375)
        Me.Return_Button.Margin = New System.Windows.Forms.Padding(0)
        Me.Return_Button.Name = "Return_Button"
        Me.Return_Button.Size = New System.Drawing.Size(800, 75)
        Me.Return_Button.TabIndex = 1
        Me.Return_Button.Text = "FERMER"
        Me.Return_Button.UseVisualStyleBackColor = True
        '
        'Panel
        '
        Me.Panel.AllowDrop = True
        Me.Panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel.Location = New System.Drawing.Point(0, 0)
        Me.Panel.Name = "Panel"
        Me.Panel.Size = New System.Drawing.Size(800, 375)
        Me.Panel.TabIndex = 2
        '
        'Options
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Panel)
        Me.Controls.Add(Me.Return_Button)
        Me.MinimumSize = New System.Drawing.Size(400, 400)
        Me.Name = "Options"
        Me.Text = "Règles"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Return_Button As Button
    Friend WithEvents Panel As Panel
End Class
