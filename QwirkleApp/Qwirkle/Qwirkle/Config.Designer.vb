﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Config
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Config))
        Me.Input_Name = New System.Windows.Forms.TableLayoutPanel()
        Me.Input_P4 = New System.Windows.Forms.TextBox()
        Me.Input_P3 = New System.Windows.Forms.TextBox()
        Me.Input_P2 = New System.Windows.Forms.TextBox()
        Me.Label_P1 = New System.Windows.Forms.Label()
        Me.Label_P2 = New System.Windows.Forms.Label()
        Me.Label_P3 = New System.Windows.Forms.Label()
        Me.Label_P4 = New System.Windows.Forms.Label()
        Me.Input_P1 = New System.Windows.Forms.TextBox()
        Me.Date_In_P1 = New System.Windows.Forms.DateTimePicker()
        Me.Date_In_P2 = New System.Windows.Forms.DateTimePicker()
        Me.Date_In_P3 = New System.Windows.Forms.DateTimePicker()
        Me.Date_In_P4 = New System.Windows.Forms.DateTimePicker()
        Me.Title_Name = New System.Windows.Forms.Label()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.Title_Token = New System.Windows.Forms.Label()
        Me.Shape_In_1 = New System.Windows.Forms.CheckBox()
        Me.Color_In_9 = New System.Windows.Forms.CheckBox()
        Me.Color_In_8 = New System.Windows.Forms.CheckBox()
        Me.Color_In_7 = New System.Windows.Forms.CheckBox()
        Me.Color_In_6 = New System.Windows.Forms.CheckBox()
        Me.Color_In_5 = New System.Windows.Forms.CheckBox()
        Me.Color_In_4 = New System.Windows.Forms.CheckBox()
        Me.Color_In_3 = New System.Windows.Forms.CheckBox()
        Me.Color_In_2 = New System.Windows.Forms.CheckBox()
        Me.Color_In_1 = New System.Windows.Forms.CheckBox()
        Me.Shape_In_9 = New System.Windows.Forms.CheckBox()
        Me.Shape_In_8 = New System.Windows.Forms.CheckBox()
        Me.Shape_In_7 = New System.Windows.Forms.CheckBox()
        Me.Shape_In_6 = New System.Windows.Forms.CheckBox()
        Me.Shape_In_5 = New System.Windows.Forms.CheckBox()
        Me.Shape_In_4 = New System.Windows.Forms.CheckBox()
        Me.Shape_In_3 = New System.Windows.Forms.CheckBox()
        Me.Shape_In_2 = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label_Shape_5 = New System.Windows.Forms.Label()
        Me.Label_Shape_9 = New System.Windows.Forms.Label()
        Me.Label_Shape_8 = New System.Windows.Forms.Label()
        Me.Label_Shape_7 = New System.Windows.Forms.Label()
        Me.Label_Shape_6 = New System.Windows.Forms.Label()
        Me.Label_Shape_4 = New System.Windows.Forms.Label()
        Me.Label_Shape_3 = New System.Windows.Forms.Label()
        Me.Label_Shape_2 = New System.Windows.Forms.Label()
        Me.Label_Shape_1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Input_Token = New System.Windows.Forms.TableLayoutPanel()
        Me.Continue_Button = New System.Windows.Forms.Button()
        Me.Config_Layout = New System.Windows.Forms.TableLayoutPanel()
        Me.Options = New System.Windows.Forms.Button()
        Me.Input_Name.SuspendLayout()
        Me.Input_Token.SuspendLayout()
        Me.Config_Layout.SuspendLayout()
        Me.SuspendLayout()
        '
        'Input_Name
        '
        Me.Input_Name.ColumnCount = 3
        Me.Input_Name.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.Input_Name.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.Input_Name.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.Input_Name.Controls.Add(Me.Input_P4, 1, 3)
        Me.Input_Name.Controls.Add(Me.Input_P3, 1, 2)
        Me.Input_Name.Controls.Add(Me.Input_P2, 1, 1)
        Me.Input_Name.Controls.Add(Me.Label_P1, 0, 0)
        Me.Input_Name.Controls.Add(Me.Label_P2, 0, 1)
        Me.Input_Name.Controls.Add(Me.Label_P3, 0, 2)
        Me.Input_Name.Controls.Add(Me.Label_P4, 0, 3)
        Me.Input_Name.Controls.Add(Me.Input_P1, 1, 0)
        Me.Input_Name.Controls.Add(Me.Date_In_P1, 2, 0)
        Me.Input_Name.Controls.Add(Me.Date_In_P2, 2, 1)
        Me.Input_Name.Controls.Add(Me.Date_In_P3, 2, 2)
        Me.Input_Name.Controls.Add(Me.Date_In_P4, 2, 3)
        Me.Input_Name.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Input_Name.Location = New System.Drawing.Point(0, 45)
        Me.Input_Name.Margin = New System.Windows.Forms.Padding(0)
        Me.Input_Name.Name = "Input_Name"
        Me.Input_Name.RowCount = 4
        Me.Input_Name.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.Input_Name.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.Input_Name.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.Input_Name.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.Input_Name.Size = New System.Drawing.Size(384, 315)
        Me.Input_Name.TabIndex = 6
        '
        'Input_P4
        '
        Me.Input_P4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Input_P4.Enabled = False
        Me.Input_P4.Location = New System.Drawing.Point(138, 264)
        Me.Input_P4.Margin = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.Input_P4.MaxLength = 16
        Me.Input_P4.Name = "Input_P4"
        Me.Input_P4.Size = New System.Drawing.Size(98, 20)
        Me.Input_P4.TabIndex = 7
        '
        'Input_P3
        '
        Me.Input_P3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Input_P3.Enabled = False
        Me.Input_P3.Location = New System.Drawing.Point(138, 185)
        Me.Input_P3.Margin = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.Input_P3.MaxLength = 16
        Me.Input_P3.Name = "Input_P3"
        Me.Input_P3.Size = New System.Drawing.Size(98, 20)
        Me.Input_P3.TabIndex = 6
        '
        'Input_P2
        '
        Me.Input_P2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Input_P2.Location = New System.Drawing.Point(138, 107)
        Me.Input_P2.Margin = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.Input_P2.MaxLength = 16
        Me.Input_P2.Name = "Input_P2"
        Me.Input_P2.Size = New System.Drawing.Size(98, 20)
        Me.Input_P2.TabIndex = 5
        '
        'Label_P1
        '
        Me.Label_P1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_P1.AutoSize = True
        Me.Label_P1.Location = New System.Drawing.Point(40, 32)
        Me.Label_P1.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_P1.Name = "Label_P1"
        Me.Label_P1.Size = New System.Drawing.Size(88, 13)
        Me.Label_P1.TabIndex = 0
        Me.Label_P1.Text = "Joueur 1:"
        '
        'Label_P2
        '
        Me.Label_P2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_P2.AutoSize = True
        Me.Label_P2.Location = New System.Drawing.Point(40, 110)
        Me.Label_P2.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_P2.Name = "Label_P2"
        Me.Label_P2.Size = New System.Drawing.Size(88, 13)
        Me.Label_P2.TabIndex = 1
        Me.Label_P2.Text = "Joueur 2:"
        '
        'Label_P3
        '
        Me.Label_P3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_P3.AutoSize = True
        Me.Label_P3.Location = New System.Drawing.Point(40, 188)
        Me.Label_P3.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_P3.Name = "Label_P3"
        Me.Label_P3.Size = New System.Drawing.Size(88, 13)
        Me.Label_P3.TabIndex = 2
        Me.Label_P3.Text = "Joueur 3:"
        '
        'Label_P4
        '
        Me.Label_P4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_P4.AutoSize = True
        Me.Label_P4.Location = New System.Drawing.Point(40, 268)
        Me.Label_P4.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_P4.Name = "Label_P4"
        Me.Label_P4.Size = New System.Drawing.Size(88, 13)
        Me.Label_P4.TabIndex = 3
        Me.Label_P4.Text = "Joueur 4:"
        '
        'Input_P1
        '
        Me.Input_P1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Input_P1.Location = New System.Drawing.Point(138, 29)
        Me.Input_P1.Margin = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.Input_P1.MaxLength = 16
        Me.Input_P1.Name = "Input_P1"
        Me.Input_P1.Size = New System.Drawing.Size(98, 20)
        Me.Input_P1.TabIndex = 4
        '
        'Date_In_P1
        '
        Me.Date_In_P1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Date_In_P1.Location = New System.Drawing.Point(259, 29)
        Me.Date_In_P1.Name = "Date_In_P1"
        Me.Date_In_P1.Size = New System.Drawing.Size(122, 20)
        Me.Date_In_P1.TabIndex = 8
        '
        'Date_In_P2
        '
        Me.Date_In_P2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Date_In_P2.Location = New System.Drawing.Point(259, 107)
        Me.Date_In_P2.Name = "Date_In_P2"
        Me.Date_In_P2.Size = New System.Drawing.Size(122, 20)
        Me.Date_In_P2.TabIndex = 9
        '
        'Date_In_P3
        '
        Me.Date_In_P3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Date_In_P3.Enabled = False
        Me.Date_In_P3.Location = New System.Drawing.Point(259, 185)
        Me.Date_In_P3.Name = "Date_In_P3"
        Me.Date_In_P3.Size = New System.Drawing.Size(122, 20)
        Me.Date_In_P3.TabIndex = 10
        '
        'Date_In_P4
        '
        Me.Date_In_P4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Date_In_P4.Enabled = False
        Me.Date_In_P4.Location = New System.Drawing.Point(259, 264)
        Me.Date_In_P4.Name = "Date_In_P4"
        Me.Date_In_P4.Size = New System.Drawing.Size(122, 20)
        Me.Date_In_P4.TabIndex = 11
        '
        'Title_Name
        '
        Me.Title_Name.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Title_Name.AutoSize = True
        Me.Title_Name.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Title_Name.Location = New System.Drawing.Point(78, 12)
        Me.Title_Name.Margin = New System.Windows.Forms.Padding(0, 12, 0, 0)
        Me.Title_Name.Name = "Title_Name"
        Me.Title_Name.Size = New System.Drawing.Size(227, 33)
        Me.Title_Name.TabIndex = 2
        Me.Title_Name.Text = "PSEUDOS DES JOUEURS"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Cancel_Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel_Button.Location = New System.Drawing.Point(92, 380)
        Me.Cancel_Button.Margin = New System.Windows.Forms.Padding(0, 20, 0, 40)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(200, 30)
        Me.Cancel_Button.TabIndex = 0
        Me.Cancel_Button.Text = "ANNULER"
        Me.Cancel_Button.UseVisualStyleBackColor = True
        '
        'Title_Token
        '
        Me.Title_Token.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Title_Token.AutoSize = True
        Me.Title_Token.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Title_Token.Location = New System.Drawing.Point(479, 12)
        Me.Title_Token.Margin = New System.Windows.Forms.Padding(0, 12, 0, 0)
        Me.Title_Token.Name = "Title_Token"
        Me.Title_Token.Size = New System.Drawing.Size(257, 33)
        Me.Title_Token.TabIndex = 4
        Me.Title_Token.Text = "CONFIGURATION DES PIONS"
        '
        'Shape_In_1
        '
        Me.Shape_In_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_1.AutoSize = True
        Me.Shape_In_1.Checked = True
        Me.Shape_In_1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Shape_In_1.Location = New System.Drawing.Point(136, 10)
        Me.Shape_In_1.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Shape_In_1.Name = "Shape_In_1"
        Me.Shape_In_1.Size = New System.Drawing.Size(56, 14)
        Me.Shape_In_1.TabIndex = 18
        Me.Shape_In_1.UseVisualStyleBackColor = True
        '
        'Color_In_9
        '
        Me.Color_In_9.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_9.AutoSize = True
        Me.Color_In_9.Location = New System.Drawing.Point(242, 290)
        Me.Color_In_9.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_9.Name = "Color_In_9"
        Me.Color_In_9.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_9.TabIndex = 35
        Me.Color_In_9.UseVisualStyleBackColor = True
        '
        'Color_In_8
        '
        Me.Color_In_8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_8.AutoSize = True
        Me.Color_In_8.Location = New System.Drawing.Point(242, 255)
        Me.Color_In_8.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_8.Name = "Color_In_8"
        Me.Color_In_8.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_8.TabIndex = 34
        Me.Color_In_8.UseVisualStyleBackColor = True
        '
        'Color_In_7
        '
        Me.Color_In_7.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_7.AutoSize = True
        Me.Color_In_7.Location = New System.Drawing.Point(242, 220)
        Me.Color_In_7.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_7.Name = "Color_In_7"
        Me.Color_In_7.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_7.TabIndex = 33
        Me.Color_In_7.UseVisualStyleBackColor = True
        '
        'Color_In_6
        '
        Me.Color_In_6.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_6.AutoSize = True
        Me.Color_In_6.Checked = True
        Me.Color_In_6.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Color_In_6.Location = New System.Drawing.Point(242, 185)
        Me.Color_In_6.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_6.Name = "Color_In_6"
        Me.Color_In_6.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_6.TabIndex = 32
        Me.Color_In_6.UseVisualStyleBackColor = True
        '
        'Color_In_5
        '
        Me.Color_In_5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_5.AutoSize = True
        Me.Color_In_5.Checked = True
        Me.Color_In_5.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Color_In_5.Location = New System.Drawing.Point(242, 150)
        Me.Color_In_5.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_5.Name = "Color_In_5"
        Me.Color_In_5.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_5.TabIndex = 31
        Me.Color_In_5.UseVisualStyleBackColor = True
        '
        'Color_In_4
        '
        Me.Color_In_4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_4.AutoSize = True
        Me.Color_In_4.Checked = True
        Me.Color_In_4.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Color_In_4.Location = New System.Drawing.Point(242, 115)
        Me.Color_In_4.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_4.Name = "Color_In_4"
        Me.Color_In_4.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_4.TabIndex = 30
        Me.Color_In_4.UseVisualStyleBackColor = True
        '
        'Color_In_3
        '
        Me.Color_In_3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_3.AutoSize = True
        Me.Color_In_3.Checked = True
        Me.Color_In_3.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Color_In_3.Location = New System.Drawing.Point(242, 80)
        Me.Color_In_3.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_3.Name = "Color_In_3"
        Me.Color_In_3.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_3.TabIndex = 29
        Me.Color_In_3.UseVisualStyleBackColor = True
        '
        'Color_In_2
        '
        Me.Color_In_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_2.AutoSize = True
        Me.Color_In_2.Checked = True
        Me.Color_In_2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Color_In_2.Location = New System.Drawing.Point(242, 45)
        Me.Color_In_2.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_2.Name = "Color_In_2"
        Me.Color_In_2.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_2.TabIndex = 28
        Me.Color_In_2.UseVisualStyleBackColor = True
        '
        'Color_In_1
        '
        Me.Color_In_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Color_In_1.AutoSize = True
        Me.Color_In_1.Checked = True
        Me.Color_In_1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Color_In_1.Location = New System.Drawing.Point(242, 10)
        Me.Color_In_1.Margin = New System.Windows.Forms.Padding(50, 3, 3, 3)
        Me.Color_In_1.Name = "Color_In_1"
        Me.Color_In_1.Size = New System.Drawing.Size(43, 14)
        Me.Color_In_1.TabIndex = 27
        Me.Color_In_1.UseVisualStyleBackColor = True
        '
        'Shape_In_9
        '
        Me.Shape_In_9.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_9.AutoSize = True
        Me.Shape_In_9.Location = New System.Drawing.Point(136, 290)
        Me.Shape_In_9.Margin = New System.Windows.Forms.Padding(40, 3, 3, 3)
        Me.Shape_In_9.Name = "Shape_In_9"
        Me.Shape_In_9.Size = New System.Drawing.Size(53, 14)
        Me.Shape_In_9.TabIndex = 26
        Me.Shape_In_9.UseVisualStyleBackColor = True
        '
        'Shape_In_8
        '
        Me.Shape_In_8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_8.AutoSize = True
        Me.Shape_In_8.Location = New System.Drawing.Point(136, 255)
        Me.Shape_In_8.Margin = New System.Windows.Forms.Padding(40, 3, 3, 3)
        Me.Shape_In_8.Name = "Shape_In_8"
        Me.Shape_In_8.Size = New System.Drawing.Size(53, 14)
        Me.Shape_In_8.TabIndex = 25
        Me.Shape_In_8.UseVisualStyleBackColor = True
        '
        'Shape_In_7
        '
        Me.Shape_In_7.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_7.AutoSize = True
        Me.Shape_In_7.Location = New System.Drawing.Point(136, 220)
        Me.Shape_In_7.Margin = New System.Windows.Forms.Padding(40, 3, 3, 3)
        Me.Shape_In_7.Name = "Shape_In_7"
        Me.Shape_In_7.Size = New System.Drawing.Size(53, 14)
        Me.Shape_In_7.TabIndex = 24
        Me.Shape_In_7.UseVisualStyleBackColor = True
        '
        'Shape_In_6
        '
        Me.Shape_In_6.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_6.AutoSize = True
        Me.Shape_In_6.Checked = True
        Me.Shape_In_6.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Shape_In_6.Location = New System.Drawing.Point(136, 185)
        Me.Shape_In_6.Margin = New System.Windows.Forms.Padding(40, 3, 3, 3)
        Me.Shape_In_6.Name = "Shape_In_6"
        Me.Shape_In_6.Size = New System.Drawing.Size(53, 14)
        Me.Shape_In_6.TabIndex = 23
        Me.Shape_In_6.UseVisualStyleBackColor = True
        '
        'Shape_In_5
        '
        Me.Shape_In_5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_5.AutoSize = True
        Me.Shape_In_5.Checked = True
        Me.Shape_In_5.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Shape_In_5.Location = New System.Drawing.Point(136, 150)
        Me.Shape_In_5.Margin = New System.Windows.Forms.Padding(40, 3, 3, 3)
        Me.Shape_In_5.Name = "Shape_In_5"
        Me.Shape_In_5.Size = New System.Drawing.Size(53, 14)
        Me.Shape_In_5.TabIndex = 22
        Me.Shape_In_5.UseVisualStyleBackColor = True
        '
        'Shape_In_4
        '
        Me.Shape_In_4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_4.AutoSize = True
        Me.Shape_In_4.Checked = True
        Me.Shape_In_4.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Shape_In_4.Location = New System.Drawing.Point(136, 115)
        Me.Shape_In_4.Margin = New System.Windows.Forms.Padding(40, 3, 3, 3)
        Me.Shape_In_4.Name = "Shape_In_4"
        Me.Shape_In_4.Size = New System.Drawing.Size(53, 14)
        Me.Shape_In_4.TabIndex = 21
        Me.Shape_In_4.UseVisualStyleBackColor = True
        '
        'Shape_In_3
        '
        Me.Shape_In_3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_3.AutoSize = True
        Me.Shape_In_3.Checked = True
        Me.Shape_In_3.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Shape_In_3.Location = New System.Drawing.Point(136, 80)
        Me.Shape_In_3.Margin = New System.Windows.Forms.Padding(40, 3, 3, 3)
        Me.Shape_In_3.Name = "Shape_In_3"
        Me.Shape_In_3.Size = New System.Drawing.Size(53, 14)
        Me.Shape_In_3.TabIndex = 20
        Me.Shape_In_3.UseVisualStyleBackColor = True
        '
        'Shape_In_2
        '
        Me.Shape_In_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Shape_In_2.AutoSize = True
        Me.Shape_In_2.Checked = True
        Me.Shape_In_2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Shape_In_2.Location = New System.Drawing.Point(136, 45)
        Me.Shape_In_2.Margin = New System.Windows.Forms.Padding(40, 3, 3, 3)
        Me.Shape_In_2.Name = "Shape_In_2"
        Me.Shape_In_2.Size = New System.Drawing.Size(53, 14)
        Me.Shape_In_2.TabIndex = 19
        Me.Shape_In_2.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Indigo
        Me.Label4.Location = New System.Drawing.Point(328, 280)
        Me.Label4.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 35)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "■"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Purple
        Me.Label2.Location = New System.Drawing.Point(328, 245)
        Me.Label2.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 35)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "■"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Pink
        Me.Label3.Location = New System.Drawing.Point(328, 210)
        Me.Label3.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 35)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "■"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Orange
        Me.Label5.Location = New System.Drawing.Point(328, 175)
        Me.Label5.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 35)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "■"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Yellow
        Me.Label7.Location = New System.Drawing.Point(328, 140)
        Me.Label7.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 35)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "■"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Brown
        Me.Label8.Location = New System.Drawing.Point(328, 105)
        Me.Label8.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 35)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "■"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(328, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 35)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "■"
        '
        'Label_Shape_5
        '
        Me.Label_Shape_5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_5.AutoSize = True
        Me.Label_Shape_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_5.Location = New System.Drawing.Point(35, 140)
        Me.Label_Shape_5.Margin = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.Label_Shape_5.Name = "Label_Shape_5"
        Me.Label_Shape_5.Size = New System.Drawing.Size(61, 35)
        Me.Label_Shape_5.TabIndex = 4
        Me.Label_Shape_5.Text = "♠"
        '
        'Label_Shape_9
        '
        Me.Label_Shape_9.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_9.AutoSize = True
        Me.Label_Shape_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_9.Location = New System.Drawing.Point(40, 280)
        Me.Label_Shape_9.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_Shape_9.Name = "Label_Shape_9"
        Me.Label_Shape_9.Size = New System.Drawing.Size(56, 35)
        Me.Label_Shape_9.TabIndex = 8
        Me.Label_Shape_9.Text = "•"
        '
        'Label_Shape_8
        '
        Me.Label_Shape_8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_8.AutoSize = True
        Me.Label_Shape_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_8.Location = New System.Drawing.Point(40, 245)
        Me.Label_Shape_8.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_Shape_8.Name = "Label_Shape_8"
        Me.Label_Shape_8.Size = New System.Drawing.Size(56, 35)
        Me.Label_Shape_8.TabIndex = 7
        Me.Label_Shape_8.Text = "∞"
        '
        'Label_Shape_7
        '
        Me.Label_Shape_7.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_7.AutoSize = True
        Me.Label_Shape_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_7.Location = New System.Drawing.Point(40, 210)
        Me.Label_Shape_7.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_Shape_7.Name = "Label_Shape_7"
        Me.Label_Shape_7.Size = New System.Drawing.Size(56, 35)
        Me.Label_Shape_7.TabIndex = 6
        Me.Label_Shape_7.Text = "▲"
        '
        'Label_Shape_6
        '
        Me.Label_Shape_6.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_6.AutoSize = True
        Me.Label_Shape_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_6.Location = New System.Drawing.Point(40, 175)
        Me.Label_Shape_6.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_Shape_6.Name = "Label_Shape_6"
        Me.Label_Shape_6.Size = New System.Drawing.Size(56, 35)
        Me.Label_Shape_6.TabIndex = 5
        Me.Label_Shape_6.Text = "★"
        '
        'Label_Shape_4
        '
        Me.Label_Shape_4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_4.AutoSize = True
        Me.Label_Shape_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_4.Location = New System.Drawing.Point(35, 105)
        Me.Label_Shape_4.Margin = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.Label_Shape_4.Name = "Label_Shape_4"
        Me.Label_Shape_4.Size = New System.Drawing.Size(61, 35)
        Me.Label_Shape_4.TabIndex = 3
        Me.Label_Shape_4.Text = "♣"
        '
        'Label_Shape_3
        '
        Me.Label_Shape_3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_3.AutoSize = True
        Me.Label_Shape_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_3.Location = New System.Drawing.Point(35, 70)
        Me.Label_Shape_3.Margin = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.Label_Shape_3.Name = "Label_Shape_3"
        Me.Label_Shape_3.Size = New System.Drawing.Size(61, 35)
        Me.Label_Shape_3.TabIndex = 2
        Me.Label_Shape_3.Text = "♦"
        '
        'Label_Shape_2
        '
        Me.Label_Shape_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_2.AutoSize = True
        Me.Label_Shape_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_2.Location = New System.Drawing.Point(35, 35)
        Me.Label_Shape_2.Margin = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.Label_Shape_2.Name = "Label_Shape_2"
        Me.Label_Shape_2.Size = New System.Drawing.Size(61, 35)
        Me.Label_Shape_2.TabIndex = 1
        Me.Label_Shape_2.Text = "♥"
        '
        'Label_Shape_1
        '
        Me.Label_Shape_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_Shape_1.AutoSize = True
        Me.Label_Shape_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Shape_1.Location = New System.Drawing.Point(40, 0)
        Me.Label_Shape_1.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label_Shape_1.Name = "Label_Shape_1"
        Me.Label_Shape_1.Size = New System.Drawing.Size(56, 35)
        Me.Label_Shape_1.TabIndex = 0
        Me.Label_Shape_1.Text = "■"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Blue
        Me.Label6.Location = New System.Drawing.Point(328, 35)
        Me.Label6.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 35)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "■"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Green
        Me.Label9.Location = New System.Drawing.Point(328, 70)
        Me.Label9.Margin = New System.Windows.Forms.Padding(40, 0, 0, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 35)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "■"
        '
        'Input_Token
        '
        Me.Input_Token.ColumnCount = 4
        Me.Input_Token.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.Input_Token.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.Input_Token.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.Input_Token.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.Input_Token.Controls.Add(Me.Label9, 3, 2)
        Me.Input_Token.Controls.Add(Me.Label6, 3, 1)
        Me.Input_Token.Controls.Add(Me.Label_Shape_1, 0, 0)
        Me.Input_Token.Controls.Add(Me.Label_Shape_2, 0, 1)
        Me.Input_Token.Controls.Add(Me.Label_Shape_3, 0, 2)
        Me.Input_Token.Controls.Add(Me.Label_Shape_4, 0, 3)
        Me.Input_Token.Controls.Add(Me.Label_Shape_6, 0, 5)
        Me.Input_Token.Controls.Add(Me.Label_Shape_7, 0, 6)
        Me.Input_Token.Controls.Add(Me.Label_Shape_8, 0, 7)
        Me.Input_Token.Controls.Add(Me.Label_Shape_9, 0, 8)
        Me.Input_Token.Controls.Add(Me.Label_Shape_5, 0, 4)
        Me.Input_Token.Controls.Add(Me.Label1, 3, 0)
        Me.Input_Token.Controls.Add(Me.Label8, 3, 3)
        Me.Input_Token.Controls.Add(Me.Label7, 3, 4)
        Me.Input_Token.Controls.Add(Me.Label5, 3, 5)
        Me.Input_Token.Controls.Add(Me.Label3, 3, 6)
        Me.Input_Token.Controls.Add(Me.Label2, 3, 7)
        Me.Input_Token.Controls.Add(Me.Label4, 3, 8)
        Me.Input_Token.Controls.Add(Me.Shape_In_2, 1, 1)
        Me.Input_Token.Controls.Add(Me.Shape_In_3, 1, 2)
        Me.Input_Token.Controls.Add(Me.Shape_In_4, 1, 3)
        Me.Input_Token.Controls.Add(Me.Shape_In_5, 1, 4)
        Me.Input_Token.Controls.Add(Me.Shape_In_6, 1, 5)
        Me.Input_Token.Controls.Add(Me.Shape_In_7, 1, 6)
        Me.Input_Token.Controls.Add(Me.Shape_In_8, 1, 7)
        Me.Input_Token.Controls.Add(Me.Shape_In_9, 1, 8)
        Me.Input_Token.Controls.Add(Me.Color_In_1, 2, 0)
        Me.Input_Token.Controls.Add(Me.Color_In_2, 2, 1)
        Me.Input_Token.Controls.Add(Me.Color_In_3, 2, 2)
        Me.Input_Token.Controls.Add(Me.Color_In_4, 2, 3)
        Me.Input_Token.Controls.Add(Me.Color_In_5, 2, 4)
        Me.Input_Token.Controls.Add(Me.Color_In_6, 2, 5)
        Me.Input_Token.Controls.Add(Me.Color_In_7, 2, 6)
        Me.Input_Token.Controls.Add(Me.Color_In_8, 2, 7)
        Me.Input_Token.Controls.Add(Me.Color_In_9, 2, 8)
        Me.Input_Token.Controls.Add(Me.Shape_In_1, 1, 0)
        Me.Input_Token.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Input_Token.Location = New System.Drawing.Point(416, 45)
        Me.Input_Token.Margin = New System.Windows.Forms.Padding(0)
        Me.Input_Token.Name = "Input_Token"
        Me.Input_Token.RowCount = 9
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.Input_Token.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Input_Token.Size = New System.Drawing.Size(384, 315)
        Me.Input_Token.TabIndex = 5
        '
        'Continue_Button
        '
        Me.Continue_Button.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Continue_Button.Enabled = False
        Me.Continue_Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Continue_Button.Location = New System.Drawing.Point(508, 380)
        Me.Continue_Button.Margin = New System.Windows.Forms.Padding(0, 20, 0, 40)
        Me.Continue_Button.Name = "Continue_Button"
        Me.Continue_Button.Size = New System.Drawing.Size(200, 30)
        Me.Continue_Button.TabIndex = 3
        Me.Continue_Button.Text = "CONTINUER"
        Me.Continue_Button.UseVisualStyleBackColor = True
        '
        'Config_Layout
        '
        Me.Config_Layout.ColumnCount = 3
        Me.Config_Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.0!))
        Me.Config_Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.0!))
        Me.Config_Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.0!))
        Me.Config_Layout.Controls.Add(Me.Options, 1, 0)
        Me.Config_Layout.Controls.Add(Me.Continue_Button, 2, 2)
        Me.Config_Layout.Controls.Add(Me.Title_Name, 0, 0)
        Me.Config_Layout.Controls.Add(Me.Cancel_Button, 0, 2)
        Me.Config_Layout.Controls.Add(Me.Input_Name, 0, 1)
        Me.Config_Layout.Controls.Add(Me.Title_Token, 2, 0)
        Me.Config_Layout.Controls.Add(Me.Input_Token, 2, 1)
        Me.Config_Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Config_Layout.Location = New System.Drawing.Point(0, 0)
        Me.Config_Layout.Name = "Config_Layout"
        Me.Config_Layout.RowCount = 3
        Me.Config_Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.Config_Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70.0!))
        Me.Config_Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.Config_Layout.Size = New System.Drawing.Size(800, 450)
        Me.Config_Layout.TabIndex = 1
        '
        'Options
        '
        Me.Options.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Options.BackgroundImage = CType(resources.GetObject("Options.BackgroundImage"), System.Drawing.Image)
        Me.Options.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Options.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Options.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.Options.FlatAppearance.BorderSize = 0
        Me.Options.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Options.Location = New System.Drawing.Point(384, 12)
        Me.Options.Margin = New System.Windows.Forms.Padding(0, 12, 0, 0)
        Me.Options.Name = "Options"
        Me.Options.Size = New System.Drawing.Size(32, 32)
        Me.Options.TabIndex = 14
        Me.Options.UseMnemonic = False
        Me.Options.UseVisualStyleBackColor = False
        Me.Options.UseWaitCursor = True
        '
        'Config
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Config_Layout)
        Me.Name = "Config"
        Me.Text = "Config"
        Me.Input_Name.ResumeLayout(False)
        Me.Input_Name.PerformLayout()
        Me.Input_Token.ResumeLayout(False)
        Me.Input_Token.PerformLayout()
        Me.Config_Layout.ResumeLayout(False)
        Me.Config_Layout.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Cancel_Button As Button
    Friend WithEvents Title_Name As Label
    Friend WithEvents Input_Name As TableLayoutPanel
    Friend WithEvents Input_P4 As TextBox
    Friend WithEvents Input_P3 As TextBox
    Friend WithEvents Input_P2 As TextBox
    Friend WithEvents Label_P1 As Label
    Friend WithEvents Label_P2 As Label
    Friend WithEvents Label_P3 As Label
    Friend WithEvents Label_P4 As Label
    Friend WithEvents Input_P1 As TextBox
    Friend WithEvents Date_In_P1 As DateTimePicker
    Friend WithEvents Date_In_P2 As DateTimePicker
    Friend WithEvents Date_In_P3 As DateTimePicker
    Friend WithEvents Date_In_P4 As DateTimePicker
    Friend WithEvents Continue_Button As Button
    Friend WithEvents Title_Token As Label
    Friend WithEvents Shape_In_1 As CheckBox
    Friend WithEvents Color_In_9 As CheckBox
    Friend WithEvents Color_In_8 As CheckBox
    Friend WithEvents Color_In_7 As CheckBox
    Friend WithEvents Color_In_6 As CheckBox
    Friend WithEvents Color_In_5 As CheckBox
    Friend WithEvents Color_In_4 As CheckBox
    Friend WithEvents Color_In_3 As CheckBox
    Friend WithEvents Color_In_2 As CheckBox
    Friend WithEvents Color_In_1 As CheckBox
    Friend WithEvents Shape_In_9 As CheckBox
    Friend WithEvents Shape_In_8 As CheckBox
    Friend WithEvents Shape_In_7 As CheckBox
    Friend WithEvents Shape_In_6 As CheckBox
    Friend WithEvents Shape_In_5 As CheckBox
    Friend WithEvents Shape_In_4 As CheckBox
    Friend WithEvents Shape_In_3 As CheckBox
    Friend WithEvents Shape_In_2 As CheckBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label_Shape_5 As Label
    Friend WithEvents Label_Shape_9 As Label
    Friend WithEvents Label_Shape_8 As Label
    Friend WithEvents Label_Shape_7 As Label
    Friend WithEvents Label_Shape_6 As Label
    Friend WithEvents Label_Shape_4 As Label
    Friend WithEvents Label_Shape_3 As Label
    Friend WithEvents Label_Shape_2 As Label
    Friend WithEvents Label_Shape_1 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Input_Token As TableLayoutPanel
    Friend WithEvents Config_Layout As TableLayoutPanel
    Friend WithEvents Options As Button
End Class
