﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Menu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Menu))
        Me.TitlePicBox = New System.Windows.Forms.PictureBox()
        Me.Play_Button = New System.Windows.Forms.Button()
        Me.App_Quit_Button = New System.Windows.Forms.Button()
        Me.Options_Button = New System.Windows.Forms.Button()
        CType(Me.TitlePicBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TitlePicBox
        '
        Me.TitlePicBox.Dock = System.Windows.Forms.DockStyle.Top
        Me.TitlePicBox.Image = CType(resources.GetObject("TitlePicBox.Image"), System.Drawing.Image)
        Me.TitlePicBox.Location = New System.Drawing.Point(0, 0)
        Me.TitlePicBox.Margin = New System.Windows.Forms.Padding(0)
        Me.TitlePicBox.Name = "TitlePicBox"
        Me.TitlePicBox.Size = New System.Drawing.Size(800, 150)
        Me.TitlePicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.TitlePicBox.TabIndex = 0
        Me.TitlePicBox.TabStop = False
        '
        'Play_Button
        '
        Me.Play_Button.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Play_Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Play_Button.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Play_Button.Location = New System.Drawing.Point(308, 200)
        Me.Play_Button.Margin = New System.Windows.Forms.Padding(0)
        Me.Play_Button.Name = "Play_Button"
        Me.Play_Button.Size = New System.Drawing.Size(200, 60)
        Me.Play_Button.TabIndex = 1
        Me.Play_Button.Text = "JOUER"
        Me.Play_Button.UseVisualStyleBackColor = True
        '
        'App_Quit_Button
        '
        Me.App_Quit_Button.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.App_Quit_Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.App_Quit_Button.Location = New System.Drawing.Point(308, 370)
        Me.App_Quit_Button.Margin = New System.Windows.Forms.Padding(0)
        Me.App_Quit_Button.Name = "App_Quit_Button"
        Me.App_Quit_Button.Size = New System.Drawing.Size(200, 60)
        Me.App_Quit_Button.TabIndex = 2
        Me.App_Quit_Button.Text = "QUITTER"
        Me.App_Quit_Button.UseVisualStyleBackColor = True
        '
        'Options_Button
        '
        Me.Options_Button.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Options_Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Options_Button.Location = New System.Drawing.Point(308, 285)
        Me.Options_Button.Margin = New System.Windows.Forms.Padding(0)
        Me.Options_Button.Name = "Options_Button"
        Me.Options_Button.Size = New System.Drawing.Size(200, 60)
        Me.Options_Button.TabIndex = 2
        Me.Options_Button.Text = "OPTIONS"
        Me.Options_Button.UseVisualStyleBackColor = True
        '
        'Menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Options_Button)
        Me.Controls.Add(Me.App_Quit_Button)
        Me.Controls.Add(Me.Play_Button)
        Me.Controls.Add(Me.TitlePicBox)
        Me.Name = "Menu"
        Me.Text = "Menu"
        CType(Me.TitlePicBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TitlePicBox As PictureBox
    Friend WithEvents Play_Button As Button
    Friend WithEvents App_Quit_Button As Button
    Friend WithEvents Options_Button As Button
End Class
